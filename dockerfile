#
# Build stage
#
FROM maven AS jeeves
ARG SRC_PATH
COPY ${SRC_PATH} ./jeeves/src/
ARG POM_FILE
COPY ${POM_FILE} ./jeeves/
WORKDIR ./jeeves/
RUN mvn -f pom.xml -Dtests.skip=true -DskipTests clean package

#
# Package stage
#
#FROM openjdk:17-alpine
FROM azul/zulu-openjdk-alpine:17-jre
COPY --from=jeeves /jeeves/target/jeeves*.jar jeeves.jar
ENTRYPOINT ["java","-jar","/jeeves.jar", "OTQyMDA2NTExMTQ2NzYyMjgw.YgeN7Q.4c64xlQ7Uvx7_0XQ1zdlf6dh5es"]
EXPOSE 8080


