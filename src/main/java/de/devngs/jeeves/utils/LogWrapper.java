package de.devngs.jeeves.utils;

import java.util.logging.Logger;

public class LogWrapper {
    private LogWrapper(){}
    static final Logger logger = Logger.getGlobal();
    public static void logInfo(String message){
        logger.info(message);
    }
    public static void logSevere(String message){
        logger.severe(message);
    }
    public static void logWarning(String message){ logger.warning(message);}
}
