package de.devngs.jeeves.bot.additionals.enums;

public enum Step {
    INITIAL,
    GAMETYPE,
    SYSTEM,
    SIZE,
    FIXEDDATE,
    DATE,
    HOW,
    LENGTH,
    SERIOUSNESS,
    TITLE,
    DESCRIPTION,
    TRIGGER,
    WHO,
    HAVEADDITIONALS,
    COLLECT,
    ADDITIONALS,
    COLLECTDATE,
    CLOSE,

    //Workshop-Stuff

    WTITLE,
    WDESC,
    WSIZE,
    WFIXDATE,
    WDATE,
    WKNOWLEDGE,
    WLENGTH,
    WCOLLECT,
    WCOLLECTDATE,
    WCLOSE
}
