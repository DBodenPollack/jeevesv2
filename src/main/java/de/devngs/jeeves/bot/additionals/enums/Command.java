package de.devngs.jeeves.bot.additionals.enums;

import de.devngs.jeeves.bot.commands.*;
import de.devngs.jeeves.bot.exceptions.BotCommandNotFoundException;
import de.devngs.jeeves.utils.LogWrapper;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.internal.interactions.CommandDataImpl;

import java.util.ArrayList;
import java.util.List;


public enum Command {

    CALC ("calc", "Errechnet mathematische Aufgaben!", new CalcCommand()),
    JEEVES ("jeeves", "Wer ist Jeeves und was kann er?", new JeevesCommand()),
    OFFER ("offer", "Spielangebot erstellen (ausschlie\u00dflich f\u00fcr @Spielleiter*in in Planung & @Spielleiter*in)", new OfferCommand()),
    PING ("ping","Pong!", new PingCommand()),
    ROLL ("roll","Roll the dices!", new RollCommand()),
    ZERBERUS ("zerberus", "?", new ZerberusCommand()),
    FEEDBACK("feedback", "Gib uns feedback!", new FeedbackCommand());


    private final String cmd;
    private final String description;
    private final ListenerAdapter listenerAdapter;

    Command (String cmd, String description, ListenerAdapter listenerAdapter ){
        this.cmd = cmd;
        this.description = description;
        this.listenerAdapter = listenerAdapter;
    }

    public String cmd() {
        return cmd;
    }
    public String description() {
        return description;
    }
    public ListenerAdapter listenerAdapter() { return listenerAdapter; }



    public static List<CommandData> getAllCommands() {
        List<CommandData> cluaList =  new ArrayList<>();

        for (Command cmd : Command.values()){
            CommandDataImpl cdi = new CommandDataImpl(cmd.cmd(), cmd.description());

            try {
                List<OptionData> optionDataList = getOptions(cmd);
                cdi.addOptions(optionDataList);
            } catch (BotCommandNotFoundException exception){

                LogWrapper.logSevere(cmd + " was skipped. No options for command found.");
            }
            cluaList.add(cdi);
        }

        return cluaList;
    }

    private static List<OptionData> getOptions(Command cmd) throws BotCommandNotFoundException {
        switch (cmd) {
            case CALC -> {
                return getCalcOptions();
            }
            case ROLL -> {
                return getRollOptions();
            }
            case ZERBERUS -> {
                return getZerberusOptions();
            }
            case FEEDBACK -> {
                return getFeedbackOptions();
            }
            default -> throw new BotCommandNotFoundException("NoValidCommand");

        }
    }

    private static List<OptionData> getZerberusOptions() {
        List<OptionData> optionDataList = new ArrayList<>();
        optionDataList.add(new OptionData(OptionType.STRING, "mode", "zerberus mode", true)
                .addChoice("Userlist", "LIST")
                .addChoice("Umfrage Erstellen", "CREATE")
                .addChoice("Umfrage Starten", "START")
                .addChoice("Umfrage Stoppen", "STOP")
                .addChoice("Umfrage-Status", "STATE")
                .addChoice("Liste aller Umfragen", "GETA")
                .addChoice("Umfrage Löschen", "DELETE")
                .addChoice("Angebote auslesen", "GETALLOFFERS")
                .addChoice("Angebot auslesen", "GETOFFERBYID")
                .addChoice("Offene Angebot auslesen", "GETOPENOFFERS")
                .addChoice("Angebote posten", "POSTALLOFFERS")
                .addChoice("Angebot posten", "POSTOFFERBYID")
                .addChoice("Angebot anpassen", "EDITOFFERBYID")
                .addChoice("Angebot löschen", "DELETEOFFERBYID"));

        optionDataList.add(new OptionData(OptionType.STRING, "qs", "Frage"));
        optionDataList.add(new OptionData(OptionType.STRING, "as", "Antworten"));
        optionDataList.add(new OptionData(OptionType.STRING, "id", "ID"));
        optionDataList.add(new OptionData(OptionType.ROLE, "r", "Rolle"));
        optionDataList.add(new OptionData(OptionType.STRING, "offer", "Offer JSON"));

        return optionDataList;
    }

    private static List<OptionData> getRollOptions() {
        List<OptionData> optionDataList = new ArrayList<>();
        optionDataList.add(new OptionData( OptionType.INTEGER, "number", "Wie viele W\u00fcrfel sollen gew\u00fcrfelt werden?", true,true ));
        optionDataList.add(new OptionData( OptionType.INTEGER, "dicesize", "Wie viele Seiten soll der W\u00fcrfel haben?", true, true ));
        optionDataList.add(new OptionData( OptionType.INTEGER, "singlemod", "Wert, der auf das Endergebnis addiert wird.", false ));
        optionDataList.add(new OptionData( OptionType.INTEGER, "eachmod", "Wert, der auf *jeden* Wurf addiert wird.", false ));
        optionDataList.add(new OptionData( OptionType.STRING, "secret", "Soll geheim gew\u00fcrfelt werden? Y/N"));

        return optionDataList;
    }

    private static List<OptionData> getCalcOptions() {
        List<OptionData> optionDataList = new ArrayList<>();
        optionDataList.add(new OptionData(OptionType.STRING, "task", "(...), + & -, * & /     hint: Bitte vor und nach Rechnungen in Klammern eine Operation setzen!", true));

        return optionDataList;
    }

    private static List<OptionData> getFeedbackOptions(){
        List<OptionData> optionDataList = new ArrayList<>();
        optionDataList.add(new OptionData(OptionType.STRING, "text", "Verbesserungen? Ver\u00e4nderungen? Nette Worte? Gern!", true));
        return optionDataList;
    }

}
