package de.devngs.jeeves.bot.additionals.enums;

public enum Server {
    MITNERMENGEFANTASIE("845071077671043073",845071077671043073L),
    DEMOCHANNEL("843523757238911027",843523757238911027L);
    private final String id;
    private final Long idAsLong;


    Server (String id, Long idAsLong ){
        this.id = id;
        this.idAsLong = idAsLong;
    }

    public Long getIdAsLong() {
        return idAsLong;
    }

    public String getId() {
        return id;
    }
}
