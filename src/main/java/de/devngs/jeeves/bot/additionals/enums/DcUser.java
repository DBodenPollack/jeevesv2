package de.devngs.jeeves.bot.additionals.enums;

public enum DcUser {
    KOKU("295938595896688640", 295938595896688640L),
    DANIEL("464415303087030282", 464415303087030282L);


    private final String id;
    private final Long idAsLong;


    DcUser(String id, Long idAsLong) {
        this.id = id;
        this.idAsLong = idAsLong;
    }

    public Long getIdAsLong() {
        return idAsLong;
    }

    public String getId() {
        return id;
    }
}
