package de.devngs.jeeves.bot.additionals.enums;

import java.awt.*;

public enum GameType {
    ONESHOT ("OneShot", Color.getHSBColor(107f,76f,100f), Channel.ONESHOTAREA),
    CAMPAIGN ("Kampagne", Color.getHSBColor(107f,76f,80f), Channel.CAMPAIGNAREA),
    WORKSHOP ("Workshop", Color.getHSBColor(284f,70f,90f), Channel.WORKSHOPAREA),
    LONGSHOT ("LongShot");

    private final String name;
    private final Color color;
    private final Channel area;

    GameType (String name, Color color, Channel area){
        this.name = name;
        this.color = color;
        this.area  = area;
    }

    GameType (String name){
        this.name = name;
        this.color = Color.BLACK;
        area = null;
    }

    public String getName(){
        return this.name;
    }
    public Color getColor(){
        return this.color;
    }
    public Channel getArea(){
        return this.area;
    }

}
