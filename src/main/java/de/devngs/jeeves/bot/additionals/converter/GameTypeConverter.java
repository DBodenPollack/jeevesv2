package de.devngs.jeeves.bot.additionals.converter;

import de.devngs.jeeves.bot.additionals.enums.GameType;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class GameTypeConverter implements AttributeConverter<GameType, String> {
    @Override
    public String convertToDatabaseColumn(GameType attribute) {
        return attribute.getName();
    }

    @Override
    public GameType convertToEntityAttribute(String dbData) {
        return GameType.valueOf(dbData);
    }
}
