package de.devngs.jeeves.bot.additionals.handler;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.utils.FileUpload;

import java.nio.charset.StandardCharsets;
import java.util.*;


public class ZerberusAdditionalsHandler {

    private ZerberusAdditionalsHandler(){
        throw new IllegalStateException("Utility class");
    }

    public static void performListCommand(SlashCommandInteractionEvent slashEvent) {
        slashEvent.reply("Following Users are here:").queue();
        byte[] csv = genereateUserListCsv(Objects.requireNonNull(slashEvent.getGuild()));
        slashEvent.getChannel().sendFiles(FileUpload.fromData(csv,"userList.csv")).queue();
    }
    private static byte[] genereateUserListCsv(Guild guild ){

        StringBuilder sb = new StringBuilder();

        sb.append("DiscordTag, Joined, Time out End, Creation Time \n");

        List<Member> memberList = guild.getMembers();
        for (Member member : memberList) {
            String comma = ",";
            String eol = "\n";
            sb.append(member.getUser().getName()).append(comma);
            sb.append(member.getTimeJoined()).append(comma);
            sb.append(member.getTimeOutEnd()).append(comma);
            sb.append(member.getTimeCreated()).append(eol);
        }
        return sb.toString().getBytes(StandardCharsets.UTF_8);
    }



}
