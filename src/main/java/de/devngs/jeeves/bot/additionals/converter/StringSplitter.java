package de.devngs.jeeves.bot.additionals.converter;

import java.util.ArrayList;
import java.util.List;

public class StringSplitter {
    private StringSplitter() {
        throw new IllegalStateException("Utility class");
    }
    public static List<String> splitString(String text, int length, int backSearch){
        int counter = 1;
        List<String> strList = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); i++, counter ++) {
            sb.append(text.charAt(i));
            if (counter == length){
                String part = sb.toString();
                String substring = part.substring(part.length() - backSearch);

                int pos = findSplitPoint(substring);
                if (pos == -1 ){
                    strList.add(part);
                } else {
                    pos = pos + 1;
                    i = i - ( part.length() - (part.length() - backSearch + pos));
                    strList.add(part.substring(0, part.length() - backSearch + pos));
                }
                counter = 0;
                sb.setLength(0);
            }
        }
        strList.add(sb.toString());
        return strList;
    }

    private static int findSplitPoint(String text) {
        int pos = 0;
        pos = text.lastIndexOf('\n');
        if (pos == -1) pos = text.lastIndexOf('.');
        if (pos == -1) pos = text.lastIndexOf('!');
        if (pos == -1) pos = text.lastIndexOf('?');
        if (pos == -1) pos = text.lastIndexOf(',');
        if (pos == -1) pos = text.lastIndexOf(' ');

        return pos;
    }
}

