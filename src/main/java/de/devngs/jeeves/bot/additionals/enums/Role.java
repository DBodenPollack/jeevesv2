package de.devngs.jeeves.bot.additionals.enums;

import lombok.Getter;

@Getter
public enum Role {
    NEWBIES ("845743671747280916","Neulinge", 845743671747280916L),
    BEGINNER ("858452330823680001", "Spiel-Einsteiger*innen", 858452330823680001L ),
    ADVANCED ("845760282768572445", "Spieler*innen",845760282768572445L ),
    ALL ("@everyone", "Jeder", 0L ),
    GAMEMASTER("845744586739810344", "Spielleiter*innen", 845744586739810344L),
    PAUSED_GAMEMASTER("966341500680155186", "Pausierte*r Spielleiter*in", 966341500680155186L),
    PLANNED_GAMEMASTER("858456152747671602", "Spielleiter*in in Planung", 858456152747671602L),
    WORKSHOP_INTERESTS("1077989256783478845", "Workshop-Interessent*in", 1077989256783478845L),

    MODERATOR( "857189461308735516", "Moderator*in", 857189461308735516L),
    OWNER("857298704364208128", "Owner", 857298704364208128L),
    DEVELOPER("1104732889184219148", "Developer", 1104732889184219148L);

    private final String id;
    private final String name;
    private final Long idAsLong;

    Role ( String id, String name, Long idAsLong ){
        this.name = name;
        this.id = id;
        this.idAsLong = idAsLong;
    }

}
