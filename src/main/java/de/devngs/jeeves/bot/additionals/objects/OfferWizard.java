package de.devngs.jeeves.bot.additionals.objects;

import de.devngs.jeeves.bot.additionals.enums.Step;
import de.devngs.jeeves.database.mysql.entity.Offer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.ChannelType;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
public class OfferWizard {
    private ChannelType channelType;
    private User user;
    private Step step;
    private Offer offer;
}
