package de.devngs.jeeves.bot.additionals.handler;

import de.devngs.jeeves.Runner;
import de.devngs.jeeves.database.mysql.entity.Survey;
import de.devngs.jeeves.database.mysql.entity.SurveyDelProb;
import de.devngs.jeeves.database.mysql.entity.SurveyVote;
import de.devngs.jeeves.database.mysql.service.interfaces.SurveyComp;
import de.devngs.jeeves.database.mysql.service.interfaces.SurveyDelProbComp;
import de.devngs.jeeves.database.mysql.service.interfaces.SurveyVoteComp;
import de.devngs.jeeves.utils.LogWrapper;
import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.components.ItemComponent;
import net.dv8tion.jda.api.interactions.components.buttons.Button;

import java.util.*;

public class SurveyHandler {

    private SurveyHandler() {
        throw new IllegalStateException("Utility class");
    }
    private static final String WRONG_ID = "Wrong ID";
    private static final Long DISCORD_ID = Runner.getServer().getIdAsLong();

    @Getter @Setter
    private static class SurveyResponse{
        private String option;
        private int count;

        @Override
        public String toString() {
            return "**Option:** " + option + "\t\t\t**Counter:** " + count;
        }
    }


    public static UUID createSurvey(SlashCommandInteractionEvent slashEvent) {
        String surveyText = Objects.requireNonNull(slashEvent.getOption("qs")).getAsString();
        String options = Objects.requireNonNull(slashEvent.getOption("as")).getAsString();
        long role = 0L;
        try {
            OptionMapping om = slashEvent.getOption("r");
            if (om != null) role = om.getAsLong();
        } catch (NullPointerException | IllegalArgumentException exception){
            LogWrapper.logWarning("no role - Ignore. Msg: " + exception.getMessage());
        }
        Survey survey = new Survey(surveyText, options, role);

        SurveyComp surveyComp = Runner.getCTX().getBean(SurveyComp.class);
        return surveyComp.saveSurvey(survey).getId();
    }

    private static boolean startPoll(Survey survey) {
        Guild g = Runner.getJda().getGuildById(DISCORD_ID);
        if (g == null) return false;

        List<Member> memberList;
        if (survey.getTargetRole() == 0L) {
            memberList = g.getMembers();
        } else {
            memberList = g.getMembersWithRoles(g.getRoleById(survey.getTargetRole()));
        }

        List<String> options = survey.getSplittedOptions();
        Collection<ItemComponent> actions = new ArrayList<>();
        for(String option: options){
            actions.add(Button.primary((survey.getId().toString() + "|" + option), option));
        }

        for (Member member : memberList) {
            try {
                member.getUser().openPrivateChannel().queue(privateChannel ->
                        privateChannel.sendMessage(survey.getSurvey())
                                .setActionRow(actions)
                                .queue()
                );
            } catch (UnsupportedOperationException | ErrorResponseException e) {

                SurveyDelProbComp surveyDelProbComp = Runner.getCTX().getBean(SurveyDelProbComp.class);
                surveyDelProbComp.saveSurveyVote(new SurveyDelProb(survey.getId(), member.getIdLong()));

                LogWrapper.logWarning(e.getMessage());
            }
        }
        return true;
    }
    private static boolean stopPoll(Survey survey) {
        Guild g = Runner.getJda().getGuildById(DISCORD_ID);
        if (g == null) return false;
        List<Member> memberList = g.getMembers();

        for (Member member : memberList) {
            try {
                member.getUser().openPrivateChannel().queue(privateChannel ->
                        privateChannel.sendMessage("Die Umfrage:\n*" + survey.getSurvey() + "*\n\nwurde geschlossen.").queue()
                );
            } catch (UnsupportedOperationException | ErrorResponseException e) {

                LogWrapper.logWarning(e.getMessage());
            }
        }
        return true;
    }


    private static List<Survey> getAllSurveys() {

        SurveyComp surveyComp = Runner.getCTX().getBean(SurveyComp.class);

        return surveyComp.findAll();
    }
    public static void performDeleteSurvey(SlashCommandInteractionEvent event) {
        try {
            deleteSurvey(event);
        } catch (Exception e) {
            LogWrapper.logWarning("Something Strange happened while deleting Votes / Survey");
            LogWrapper.logWarning("[ERROR]:" + e.getMessage());
            event.reply("Deletion could not be performed.").queue();
        }
    }



    public static void performCreateCommand(SlashCommandInteractionEvent slashEvent) {
        UUID surveyID = createSurvey(slashEvent);
        slashEvent.reply("Survey ID: " + surveyID.toString()).queue();
    }

    public static void performGetAllCommand(SlashCommandInteractionEvent slashEvent) {
        List<Survey> surveyList = getAllSurveys();
        if (surveyList.isEmpty()) {
            slashEvent.reply("No Events found").queue();
            return;
        }
        for(Survey survey : surveyList){
            slashEvent.getChannel().sendMessage(survey.toStringWithBreaks()).queue();
        }
        slashEvent.reply("Done").setEphemeral(true).queue();
    }

    public static void startSurvey(SlashCommandInteractionEvent slashEvent) {
        String surveyId = Objects.requireNonNull(slashEvent.getOption("id")).getAsString();
        SurveyComp surveyComp = Runner.getCTX().getBean(SurveyComp.class);
        Optional<Survey> oSurvey = surveyComp.findSurveyById(UUID.fromString(surveyId));
        if (oSurvey.isPresent()) {
            Survey survey = oSurvey.get();
            if (survey.isActive()){
                slashEvent.reply("Survey already started.").queue();
                return;
            }
            if(startPoll(survey)){
                survey.setActive(true);
                surveyComp.saveSurvey(survey);
            }

            slashEvent.reply("Survey started. User informed").queue();
        } else {
            slashEvent.reply(WRONG_ID).queue();
        }
    }

    public static void getSurveyState(SlashCommandInteractionEvent slashEvent) {
        String surveyId = Objects.requireNonNull(slashEvent.getOption("id")).getAsString();
        SurveyVoteComp surveyVoteComp = Runner.getCTX().getBean(SurveyVoteComp.class);
        List<SurveyVote> voteList = surveyVoteComp.findBySurveyId(UUID.fromString(surveyId));

        if( !voteList.isEmpty()){
            SurveyComp surveyComp = Runner.getCTX().getBean(SurveyComp.class);
            Optional<Survey> osurvey = surveyComp.findSurveyById(UUID.fromString(surveyId));
            assert osurvey.isPresent();
            Survey survey = osurvey.get();
            List<String> options = survey.getSplittedOptions();

            List<SurveyResponse> surveyResponses = new ArrayList<>();
            for (String option : options){
                SurveyResponse sr = new SurveyResponse();
                sr.setOption(option);
                int i = 0;
                for(SurveyVote surveyVote : voteList){
                    if (surveyVote.getUsedOption().equals(option)){
                        i++;
                    }
                }
                sr.count = i;
                surveyResponses.add(sr);
            }
            StringBuilder reply = new StringBuilder();
            for (SurveyResponse ret : surveyResponses){
                reply.append(ret.toString()).append("\r");
            }
            slashEvent.reply(reply.toString()).queue();

        } else {
            slashEvent.reply(WRONG_ID).queue();
        }
    }

    public static void stopSurvey(SlashCommandInteractionEvent slashEvent) {
        String surveyId = Objects.requireNonNull(slashEvent.getOption("id")).getAsString();
        SurveyComp surveyComp = Runner.getCTX().getBean(SurveyComp.class);
        Optional<Survey> oSurvey = surveyComp.findSurveyById(UUID.fromString(surveyId));
        if (oSurvey.isPresent()) {
            Survey survey = oSurvey.get();
            if (!survey.isActive()){
                slashEvent.reply("Survey is not activated.").queue();
                return;
            }
            if(stopPoll(survey)) {
                survey.setActive(false);
                surveyComp.saveSurvey(survey);
            }
            slashEvent.reply("Survey stopped. User informed").queue();
        } else {
            slashEvent.reply(WRONG_ID).queue();
        }
    }


    public static void setUserState(ButtonInteractionEvent event) {
        //button should have the following structure: [SurveyUUID]|[OPTION]
        String buttonId = event.getButton().getId();
        assert buttonId != null;
        if(!buttonId.isEmpty() && !buttonId.contains("|")) return;
        String[] butParms = buttonId.split("\\|");
        UUID surveyId = UUID.fromString(butParms[0]);
        String usedOption = butParms[1];

        SurveyComp surveyComp = Runner.getCTX().getBean(SurveyComp.class);
        Optional<Survey> os = surveyComp.findSurveyById(surveyId);
        assert os.isPresent();
        Survey s = os.get();
        if(s.isActive()){
            SurveyVoteComp surveyVoteComp = Runner.getCTX().getBean(SurveyVoteComp.class);
            SurveyVote sv = new SurveyVote(surveyId, event.getUser().getIdLong(), usedOption);

            surveyVoteComp.saveSurveyVote(sv);
            event.reply("**" + usedOption + "**" + " Wurde als Stimme gespeichert.\n Möchtest du deinen Wert ändern - Dr\u00fccke einfach einen anderen Button!").queue();
        } else {
            event.reply("Diese umfrage wurde bereits geschlossen.").queue();
        }
    }

    private static void deleteSurvey(SlashCommandInteractionEvent slashEvent){
        String surveyId = Objects.requireNonNull(slashEvent.getOption("id")).getAsString();
        SurveyComp surveyComp = Runner.getCTX().getBean(SurveyComp.class);
        SurveyVoteComp surveyVoteComp = Runner.getCTX().getBean(SurveyVoteComp.class);
        Optional<Survey> oSurvey = surveyComp.findSurveyById(UUID.fromString(surveyId));
        if (oSurvey.isPresent()) {
            Survey survey = oSurvey.get();
            if( survey.isActive() ){
                slashEvent.reply("Survey is going on. Cannot be deleted while its active.").queue();
                return;
            }
            surveyVoteComp.deleteAllVotesBySurveyId(survey.getId());
            surveyComp.deleteSurvey(survey);
            slashEvent.reply("Survey Deleted.").queue();
        } else {
            slashEvent.reply(WRONG_ID).queue();
        }
    }
}
