package de.devngs.jeeves.bot.additionals.handler;

import de.devngs.jeeves.Runner;
import de.devngs.jeeves.bot.additionals.Helper;
import de.devngs.jeeves.bot.additionals.converter.StringSplitter;
import de.devngs.jeeves.bot.additionals.enums.Channel;
import de.devngs.jeeves.bot.additionals.enums.GameType;
import de.devngs.jeeves.bot.additionals.enums.Role;
import de.devngs.jeeves.database.mysql.entity.Offer;
import de.devngs.jeeves.database.mysql.entity.OfferMessages;
import de.devngs.jeeves.database.mysql.service.interfaces.OfferComp;
import de.devngs.jeeves.database.mysql.service.interfaces.OfferMessageComp;
import de.devngs.jeeves.handler.ProblemHandler;
import de.devngs.jeeves.utils.LogWrapper;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.channel.concrete.Category;
import net.dv8tion.jda.api.entities.channel.concrete.ForumChannel;
import net.dv8tion.jda.api.entities.channel.concrete.NewsChannel;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.requests.restaction.ChannelAction;
import net.dv8tion.jda.api.utils.FileUpload;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.springframework.core.io.DefaultResourceLoader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;


public class OfferHandler {
    private static final String WRONG_ID = "Die gegebene ID war Fehlerhaft.";
    private static final Integer MAX_MESSAGE_LENGTH = 1990;

    private OfferHandler() {
        throw new IllegalStateException("Utility class");
    }

    public static void getAllOffers(SlashCommandInteractionEvent event) {
        OfferComp offerComp = Runner.getCTX().getBean(OfferComp.class);
        List<Offer> offerList = offerComp.findAll();

        for (Offer offer : offerList) {
            sendOfferAsJsonOrFile(event, offer);
        }
        event.reply("Done").setEphemeral(true).queue();
    }

    private static void sendOfferAsJsonOrFile(SlashCommandInteractionEvent event, Offer offer) {
        String ofStr = offer.toAdminString();
        if (ofStr.length() > 1999) {
            event.getChannel().sendFiles(FileUpload.fromData(ofStr.getBytes(StandardCharsets.UTF_8), (offer.getOfferId() + ".json"))).queue();
        } else {
            event.getChannel().sendMessage("```json\n" + offer.toAdminString() + "```").queue();
        }
    }

    public static void getOpenOffers(SlashCommandInteractionEvent event) {
        OfferComp offerComp = Runner.getCTX().getBean(OfferComp.class);
        List<Offer> offerList = offerComp.findByPosted(false);

        for (Offer offer : offerList) {
            sendOfferAsJsonOrFile(event, offer);
        }
        event.reply("Done").setEphemeral(true).queue();
    }

    public static void getOfferByID(SlashCommandInteractionEvent event, String id) {
        OfferComp offerComp = Runner.getCTX().getBean(OfferComp.class);
        Optional<Offer> oo;
        try {
            oo = offerComp.findOfferById(UUID.fromString(id));
        } catch (IllegalArgumentException exception) {
            LogWrapper.logWarning(exception.getMessage());
            event.reply(WRONG_ID).queue();
            return;
        }
        if (oo.isEmpty()) {
            event.reply(WRONG_ID).setEphemeral(true).queue();
            return;
        }
        sendOfferAsJsonOrFile(event, oo.get());
    }

    public static void postAllOffers(SlashCommandInteractionEvent event) {
        OfferComp offerComp = Runner.getCTX().getBean(OfferComp.class);
        List<Offer> offerList = offerComp.findByPosted(false);
        for (Offer offer : offerList) {
            if (!postOffer(event, offer)) {
                event.reply("was not able to post offer with ID: "+ offer.getOfferId()).setEphemeral(true).queue();
                continue;
            }
            setOfferAsPosted(offer);
        }
        event.reply("done").queue();
    }

    private static boolean postOffer(SlashCommandInteractionEvent event, Offer offer) {
        boolean unsuccess = false;
        Guild guild = event.getGuild();
        if( guild == null ) {
            event.reply("Fehler beim finden der Guild").queue();
            return unsuccess;
        }

        NewsChannel newsChannel = getNewsChannel(guild, offer.getGameType());
        if( newsChannel == null ) {
            ProblemHandler.contactMods(guild, "Spielangebots-Channel nicht gefunden, Angebot mit ID \"" + offer.getOfferId() + "\" konnte nicht geposted werden.");
            return unsuccess;
        }

        List<String> offerParts = StringSplitter.splitString(offer.toString(), 1900, 100);
        for (String offerPart : offerParts) {
            newsChannel.sendMessage(offerPart).queue();
        }
        try {
            createRoundChannel(guild, offer);
        } catch (IOException e) {
            ProblemHandler.contactMods(guild, "Spielangebots-Angebot mit ID \"" + offer.getOfferId() + "\" konnte nicht geposted werden.");
            throw new RuntimeException(e);
        }
        return true;
    }
    private static void setOfferAsPosted(Offer offer){
        OfferComp offerComp = Runner.getCTX().getBean(OfferComp.class);
        offer.setPosted(true);
        offerComp.saveOffer(offer);
    }

    private static NewsChannel getNewsChannel(Guild guild, GameType gameType) {
        switch (gameType) {
            case ONESHOT, CAMPAIGN -> {
                return guild.getNewsChannelById(Channel.OFFERS.getIdAsLong());
            }
            case WORKSHOP -> {
                return guild.getNewsChannelById(Channel.WORKSHOPS.getIdAsLong());
            }
            default -> {
                return null;
            }
        }
    }



    private static void updateOfferMessageAndOfferTable(Message message, Offer offer) {
        OfferMessageComp offerMessageComp = Runner.getCTX().getBean(OfferMessageComp.class);

        OfferMessages om = new OfferMessages();
        om.setChannelId(message.getChannel().getIdLong());
        om.setMessageId(message.getIdLong());
        om.setOfferId(offer.getOfferId());

        offerMessageComp.saveOfferMessage(om);
    }

    private static void createRoundChannel(Guild guild, Offer offer) throws IOException {
        Category area =  guild.getCategoryById(offer.getGameType().getArea().getIdAsLong());
        assert area != null;

        ChannelAction<ForumChannel> gameForum = area.createForumChannel(offer.getTitle())
                .setDefaultLayout(ForumChannel.Layout.GALLERY_VIEW)
                .addPermissionOverride(guild.getPublicRole(), null, EnumSet.of(Permission.VIEW_CHANNEL))
                .addPermissionOverride(Objects.requireNonNull(guild.getRoleById(Role.MODERATOR.getIdAsLong())), EnumSet.of(Permission.VIEW_CHANNEL), null)
                .addPermissionOverride(Objects.requireNonNull(guild.getMemberById(offer.getGameMaster())), EnumSet.of(Permission.VIEW_CHANNEL, Permission.MESSAGE_MANAGE), null)
                .setTopic("Dies ist euer Space! Richtet ihn euch ein, wie ihr ihn braucht!");


        //FileUpload dice.gif = FileUpload.fromData(new DefaultResourceLoader().getResource("classpath:META-INF/dice.gif").getFile(), "dice.gif.gif");
        String diceText = new DefaultResourceLoader().getResource("classpath:META-INF/DiceText").getContentAsString(StandardCharsets.UTF_8);
        //FileUpload calendar.gif = FileUpload.fromData(new DefaultResourceLoader().getResource("classpath:META-INF/calendar.gif").getFile(), "calendar.gif.gif");
        String calendarText = new DefaultResourceLoader().getResource("classpath:META-INF/CalendarText").getContentAsString(StandardCharsets.UTF_8);

        gameForum.queue(forumChannel -> {
            forumChannel.createForumPost("Terminkalender", MessageCreateData.fromContent(calendarText)).queue();
            /*
            message -> {
                message.getMessage().editMessageAttachments(calendar.gif).queue();
            }
            */
            forumChannel.createForumPost("Würfelteller", MessageCreateData.fromContent(diceText)).queue();
            /*message ->{
                message.getMessage().editMessageAttachments(dice.gif).queue();
            }
            */
            List<String> offerParts = StringSplitter.splitString(offer.toChannelDescString(), 1900, 100);
            String first = offerParts.get(0);
            offerParts.remove(0);
            forumChannel.createForumPost("Abenteuer", MessageCreateData.fromContent(first)).queue(
                    entry -> {
                        for(String offerpart: offerParts){
                            entry.getThreadChannel().sendMessage(offerpart).queue();
                        }

                    }
            );


        });

    }

    public static void postOfferByID(SlashCommandInteractionEvent event, String id) {
        OfferComp offerComp = Runner.getCTX().getBean(OfferComp.class);
        Optional<Offer> oo;
        try {
            oo = offerComp.findOfferById(UUID.fromString(id));
        } catch (IllegalArgumentException exception) {
            LogWrapper.logWarning(exception.getMessage());
            event.reply(WRONG_ID).queue();
            return;
        }

        if (oo.isEmpty()) {
            event.reply(WRONG_ID).setEphemeral(true).queue();
            return;
        }
        Offer offer = oo.get();
        if (!postOffer(event, offer)) {
            event.reply("was not able to post offer").setEphemeral(true).queue();
            return;
        }
        setOfferAsPosted(offer);

        event.reply("Done").setEphemeral(true).queue();
    }

    public static void editOfferByID(SlashCommandInteractionEvent event, String id, String offer) {
        OfferComp offerComp = Runner.getCTX().getBean(OfferComp.class);
        Optional<Offer> oo;
        try {
            oo = offerComp.findOfferById(UUID.fromString(id));
        } catch (IllegalArgumentException exception) {
            LogWrapper.logWarning(exception.getMessage());
            event.reply(WRONG_ID).queue();
            return;
        }

        if (oo.isEmpty()) {
            event.reply("Offer mit der ID" + id + "wurde nicht gefunden.").queue();
        } else {
            Offer importOffer = Offer.jsonToOffer(offer);
            if (Objects.equals(importOffer.getOfferId().toString(), id)) {
                offerComp.saveOffer(importOffer);
                event.reply("Offer geupdated!.").queue();
            } else {
                event.reply("Offer ID stimmt nicht mit dem JSON überein.").queue();
            }
        }
    }

    public static void deleteOfferByID(SlashCommandInteractionEvent event, String id) {
        OfferComp offerComp = Runner.getCTX().getBean(OfferComp.class);
        try {
            offerComp.deleteOfferById(UUID.fromString(id));
            event.reply("Done").queue();
        } catch (IllegalArgumentException exception) {
            LogWrapper.logWarning(exception.getMessage());
            event.reply(WRONG_ID).queue();
        }
    }
}
