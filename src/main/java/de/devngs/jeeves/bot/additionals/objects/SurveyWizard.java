package de.devngs.jeeves.bot.additionals.objects;

import de.devngs.jeeves.database.mysql.entity.Survey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.dv8tion.jda.api.entities.User;


@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
public class SurveyWizard {
    private User user;
    private Survey survey;
    private String joice;
}
