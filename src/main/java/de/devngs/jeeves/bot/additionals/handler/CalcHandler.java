package de.devngs.jeeves.bot.additionals.handler;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class CalcHandler {
    private CalcHandler() {
        throw new IllegalStateException("Utility class");
    }

    public static String calc(final String toCalc){
        String math =  toCalc.toLowerCase(Locale.ROOT)
                .replaceAll( "\\s" , "")
                .replace(",", ".")
                .replace("x", "*")
                .replace("w", "d");

        while (Pattern.compile("[-+*/()d^]").matcher(math).find()) {
            if(math.contains("(") || math.contains(")")){
                int firstIndex = math.lastIndexOf('(');
                int lastIndex = math.indexOf(')', firstIndex);

                String subTask = math.substring(firstIndex, lastIndex + 1);
                String subErg = solveTask(subTask);
                math = math.replace(subTask, subErg);
            } else {
                String subErg = solveTask(math);
                math = math.replace(math, subErg);
            }
        }
        return math;
    }

    private static String solveTask(final String toCalc){

        String calc = toCalc.replace("(","")
                .replace(")","" );

        List<String> calcList = new LinkedList<>(Arrays.asList(calc.split("((?<=[-+*/d^])|(?=[-+*/d^]))")));

        while(calcList.size() > 1){
            int index = 0;
            String erg = "";

            if(calcList.contains("d")){
                index = calcList.indexOf("d");
                Integer number = Integer.parseInt(calcList.get(index-1));
                Integer dice = Integer.parseInt(calcList.get(index+1));

                Integer iErg = 0;
                for (int i = 0; i < number; i++){
                    iErg += DiceHandler.rollDice(1, dice);
                }
                erg = iErg.toString();
            } else if (calcList.contains("^")){
                index = calcList.indexOf("^");
                erg = String.valueOf(Math.pow(Double.parseDouble(calcList.get(index-1)), Double.parseDouble(calcList.get(index+1))));
            } else if (calcList.contains("/")){
                index = calcList.indexOf("/");
                erg =  String.valueOf(Double.parseDouble(calcList.get(index-1)) / Double.parseDouble(calcList.get(index+1)));
            } else if (calcList.contains("*")){
                index = calcList.indexOf("*");
                erg =  String.valueOf(Double.parseDouble(calcList.get(index-1)) * Double.parseDouble(calcList.get(index+1)));
            } else if (calcList.contains("-")){
                index = calcList.indexOf("-");
                erg = String.valueOf(Double.parseDouble(calcList.get(index-1)) - Double.parseDouble(calcList.get(index+1)));
            } else if (calcList.contains("+")){
                index = calcList.indexOf("+");
                erg = String.valueOf(Double.parseDouble(calcList.get(index-1)) + Double.parseDouble(calcList.get(index+1)));
            }

            calcList.set(index, erg);
            calcList.remove(index+1);
            calcList.remove(index-1);
        }

        return calcList.toString().replaceAll("[]\\[]", "");
    }
}
