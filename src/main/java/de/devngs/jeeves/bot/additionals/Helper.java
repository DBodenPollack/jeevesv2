package de.devngs.jeeves.bot.additionals;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.random.RandomGenerator;

public class Helper {

    private Helper() {
        throw new IllegalStateException("Utility class");
    }
    public static Integer getRandomInt ( final int min, final int max ){
        RandomGenerator rg = RandomGenerator.of("L64X256MixRandom");
        return rg.nextInt(min,  (max + min));
    }

    public static Role findRole(final Member member, final String roleId) {
        List<Role> roles = member.getRoles();
        return roles.stream()
                .filter(role -> role.getId().equals(roleId)) // filter by role name
                .findFirst() // take first result
                .orElse(null); // else return null
    }

    public static boolean userHaveRoles(final Member member, final List<String> roleIds) {
        for (String roleId : roleIds) {
            boolean ret = userHaveRole(member, roleId);
            if (!ret) {
                return false;
            }
        }
        return true;
    }

    public static boolean isInRoles(final Member member, final List<String> roleIds) {
        for (String roleId: roleIds) {
            if( userHaveRole(member, roleId)) return true;
        }
        return false;
    }

    public static boolean userHaveRole(final Member member, final String roleId) {
        List<Role> roles = member.getRoles();
        return roles.stream()
                .filter(role -> role.getId().equals(roleId))
                .findFirst()
                .orElse(null) != null;
    }

    public static String[] splitToNChar(final String text, final int size) {
        List<String> parts = new ArrayList<>();

        int length = text.length();
        for (int i = 0; i < length; i += size) {
            parts.add(text.substring(i, Math.min(length, i + size)));
        }
        return parts.toArray(new String[0]);
    }
}