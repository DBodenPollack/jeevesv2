package de.devngs.jeeves.bot.additionals.converter;

import de.devngs.jeeves.bot.additionals.enums.Channel;
import jakarta.persistence.AttributeConverter;

import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class ChannelConverter implements AttributeConverter<Channel, Long> {
    @Override
    public Long convertToDatabaseColumn(Channel attribute) {
        return attribute.getIdAsLong();
    }

    @Override
    public Channel convertToEntityAttribute(Long dbData) {
        return Channel.valueOf(dbData.toString());
    }
}
