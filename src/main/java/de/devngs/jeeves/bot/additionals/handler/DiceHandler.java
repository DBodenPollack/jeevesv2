package de.devngs.jeeves.bot.additionals.handler;

import de.devngs.jeeves.bot.additionals.Helper;

public class DiceHandler {
    private DiceHandler() {
        throw new IllegalStateException("Utility class");
    }

    public static Integer rollDice(final int min, final int max){
        return Helper.getRandomInt(min,max);
    }
}
