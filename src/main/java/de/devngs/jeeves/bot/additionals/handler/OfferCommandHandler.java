package de.devngs.jeeves.bot.additionals.handler;

import de.devngs.jeeves.Runner;
import de.devngs.jeeves.bot.additionals.Helper;
import de.devngs.jeeves.bot.additionals.converter.StringSplitter;
import de.devngs.jeeves.bot.additionals.enums.Channel;
import de.devngs.jeeves.bot.additionals.enums.GameType;
import de.devngs.jeeves.bot.additionals.enums.Role;
import de.devngs.jeeves.bot.additionals.enums.Server;
import de.devngs.jeeves.bot.additionals.objects.OfferWizard;
import de.devngs.jeeves.database.mysql.entity.Offer;
import de.devngs.jeeves.database.mysql.service.interfaces.OfferComp;
import de.devngs.jeeves.utils.LogWrapper;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import static de.devngs.jeeves.bot.additionals.enums.Step.*;

public class OfferCommandHandler {

    private static final String OFFERIDTEXT = "ID des Angebotes: ";
    private static final String BUTTONCOLLECT = "collect";
    private static final String BUTTONINSTANT = "instant";
    private static final String BUTTONSAMMELN = "Sammeln";
    private static final String BUTTONSCHNELL = "Schnell";
    private static final String CODEMARKER = "```" ;
    private static final String BIGLINEBREAK = " \n\n";

    private OfferCommandHandler() {
        throw new IllegalStateException("Utility class");
    }
    public static void setGameTypeTriggerSystem(ButtonInteractionEvent event, OfferWizard offerWizard) {
        logInfo("setGameTypeTriggerSystem by:" + event.getUser());
        String type = event.getButton().getId();
        switch (Objects.requireNonNull(type)) {
            case "campaign" -> offerWizard.getOffer().setGameType(GameType.CAMPAIGN);
            case "oneshot" -> offerWizard.getOffer().setGameType(GameType.ONESHOT);
            case "workshop" -> {
                triggerWorkshopStuff(event, offerWizard);
                return;
            }
            default -> {
                return;
            }
        }
        event.reply(
                "Gut, dann fangen wir mal an, alle Informationen f\u00fcr dein Abenteuer als " + offerWizard.getOffer().getGameType().getName() + " zu sammeln!\n\n" +
                        "**Gib nun in den Chat hier ein, mit welchem Spielsystem du arbeiten m\u00f6chtest.**\n" +
                        "Beispiel: *Call of Cthulhu 7e* oder *DnD 5e*\n" +
                        "Wie du es schreibst ist egal. Dieser Text wird in dem Spielangebot hinter \"Spielsystem\" stehen.").queue();
        offerWizard.setStep(SYSTEM);
    }

    private static void triggerWorkshopStuff(ButtonInteractionEvent event, OfferWizard offerWizard) {
        offerWizard.getOffer().setGameType(GameType.WORKSHOP);
        event.reply(
                """
                        Gut, dann fangen wir mal an, alle Informationen f\u00fcr dein Workshop zu sammeln!
                        **Gib nun hier in den Chat den Titel des Workshops ein**
                        Beispiel: *Spielleiter/innen Workshop für Einsteiger/innen*
                        Wie du es schreibst ist egal. Dieser Text wird in dem Workshopangebot hinter "Workshopinhalte" stehen.
                        """).queue();
        offerWizard.setStep(WTITLE);

    }

    public static void setSystemTriggerSize(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setSystemTriggerSize by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setGameSystem(message);

        event.getMessage().reply(
                        message + " wurde als Spielsystem gespeichert!\n\n" +
                                "**Nun - mit wie vielen Mitspielenden m\u00f6chtest du spielen?**\n" +
                                "Beispiel: *5*, *2-9*, *3*")
                .queue();
        offerWizard.setStep(SIZE);
    }

    public static void setSizeTriggerFixedDate(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setSizeTriggerFixedDate by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setSize(message);
        event.getMessage().reply(
                        message + " Mitspielende sollen also teilnehmen.\n\n" +
                                "**Findet das Abenteuer an einem festen Termin statt?**\n\n" +
                                "Klicke hier auch auf \"Ja\", wenn du beispielsweise einen regelmäßigen festen Termin hast.\n" +
                                "Beispiel: *jede Woche donnerstags*"

                )
                .setActionRow(
                        Button.success("yes", "Ja"),
                        Button.danger("no", "Nein")
                )
                .queue();
        offerWizard.setStep(FIXEDDATE);
    }

    public static void setFixedDateAndTriggerDateOrHow(ButtonInteractionEvent event, OfferWizard offerWizard) {
        logInfo("setFixedDateAndTriggerDateOrHow by: " + event.getUser());
        switch (Objects.requireNonNull(event.getButton().getId())) {
            case "yes" -> {
                offerWizard.getOffer().setFixedDate(true);
                event.reply(
                        """
                                Du möchtest an einem festen Tag spielen.

                                **Gib nun das Datum ein, an dem gespielt werden soll.**
                                Beispiel: *2. Juli 2022  20:30 Uhr*, *27.12.2022*, *3.Mai*, *hauptsache nach 16 Uhr*"""
                ).queue();
                offerWizard.setStep(DATE);
            }
            case "no" -> {
                offerWizard.getOffer().setFixedDate(false);
                event.reply(
                        "Wie m\u00f6chtest du Spielen?\n" +
                                "Beispiel: Foundry, Roll20, Discord mit Kamera und und und..."
                ).queue();
                offerWizard.setStep(HOW);
            }
        }
    }

    public static void setDateTriggerHow(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setDateTriggerHow by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setPlayDate(message);
        event.getMessage().reply(
                message + " wurde als Datum gespeichert. Weiter geht's:\n\n" +
                        "**Wie wird gespielt?**\n" +
                        "Beispiel: *Foundry*, *Roll20*, *Discord mit Kamera* und und und..."
        ).queue();
        offerWizard.setStep(HOW);
    }

    public static void setHowTriggerLength(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setHowTriggerLength by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setHowPlay(message);
        event.getMessage().reply(
                "Du m\u00f6chtest also wie folgt spielen: " + message + BIGLINEBREAK +
                        "**Was denkst du, wie lang wird das Abenteuer in etwa gehen?**\n" +
                        "Beispiel: *3-4 Abende \u00e1 4 Stunden*, *in Summe etwa 90 Stunden*, *OneShot an einem Abend 3-6 Stunden*..."
        ).queue();
        offerWizard.setStep(LENGTH);
    }

    public static void setLengthTriggerSeriousness(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setLengthTriggerSeriousness by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setLength(message);

        event.getMessage().reply(
                        "Für die Länge deines Abenteuers hast du folgendes angegeben: " + message + BIGLINEBREAK +
                                "**Wie ernsthaft soll es in deinem Abenteuer zugehen?\n**" +
                                "Beispiel: *lustiges Abenteuer*, *lustige Welt*, *ernstes Abenteuer*, *ernste Welt & lustige Spieler/innen*...")
                .queue();

        offerWizard.setStep(SERIOUSNESS);
    }

    public static void setSeriousnessTriggerTitel(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setSeriousnessTriggerTitel by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setSeriousness(message);

        event.getMessage().reply(
                        "Ernsthaftigkeit: " + message + BIGLINEBREAK +
                                "**Welchen Titel soll dein Abenteuer tragen?**")
                .queue();

        offerWizard.setStep(TITLE);
    }

    public static void setTitleTriggerDescription(@NotNull MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setTitleTriggerDescription by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setTitle(message);

        event.getMessage().reply("Dein Abenteuer tr\u00e4gt den Titel: " + message + BIGLINEBREAK +
                        "**Wie würdest du das Setting deines Abenteuers beschreiben?**\n" +
                        "*Optional kannst du auch ein Abenteuer-Intro angeben.*")
                .queue();

        offerWizard.setStep(DESCRIPTION);
    }

    public static void setDescriptionTriggerTrigger(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setDescriptionTriggerTrigger by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setDescript(message);

        event.getMessage().reply("""
                        Spannend!

                        **Gibt es in deinem Abenteuer Dinge, mit denen du jemanden triggern k\u00f6nntest?**
                        **Wenn ja, welche?**
                        Beispiel: *Spinnen*, *Rassismus*, *Antisemitismus*, *Gore* ...

                        *Hinweis: Sollte es keine geben - schreibe einfach "Keine"*""")
                .queue();

        offerWizard.setStep(TRIGGER);
    }

    public static void setTriggerTriggerWho(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setTriggerTriggerHowJoin by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setTriggerList(message);
        logInfo("setHowJoinTriggerWho by: " + event.getAuthor());
        event.getMessage().reply(
                        "Deine Trigger: " + message + "\n\n" +
                        "**Welche niedrigste Spielerfahrung m\u00f6chtest du f\u00fcr dein Spiel einladen?**\n" +
                                "*Hinweis: Spieler/innen haben in der Regel bereits ein paar Runden hinter sich.*"
                )
                .setActionRow(
                        Button.primary("newbies", Role.NEWBIES.getName()),
                        Button.primary("beginner", Role.BEGINNER.getName()),
                        Button.primary("advanced", Role.ADVANCED.getName())/*,
                        Button.primary("all", "Jeder (Everyone)!")*/
                )
                .queue();
        offerWizard.setStep(WHO);

    }

    public static void setWhoTriggerHaveAdditionals(ButtonInteractionEvent event, OfferWizard offerWizard) {
        logInfo("setWhoTriggerHaveAdditionals by: " + event.getUser());
        switch (Objects.requireNonNull(event.getButton().getId())) {
            case "newbies" -> offerWizard.getOffer().setWhoCanJoin(Role.NEWBIES);
            case "beginner" -> offerWizard.getOffer().setWhoCanJoin(Role.BEGINNER);
            case "advanced" -> offerWizard.getOffer().setWhoCanJoin(Role.ADVANCED);
            case "all" -> offerWizard.getOffer().setWhoCanJoin(Role.ALL);
            default -> {
                return;
            }
        }

        event.reply(
                        "**Hast du zus\u00e4tzliche Informationen f\u00fcr die Spieler*innen?** \n" +
                                "Beispiel: *mindestens 18 Jahre*, *Erfahrungen in XY*... "
                )
                .addActionRow(
                        Button.success("yes", "Ja"),
                        Button.danger("no", "Nein")
                )
                .queue();
        offerWizard.setStep(HAVEADDITIONALS);
    }

    public static void setHaveAdditionalsTriggerAdditionalsOrCollect(ButtonInteractionEvent event, OfferWizard offerWizard) {
        logInfo("setHaveAdditionalsTriggerAdditionalsOrCollect by: " + event.getUser());
        switch (Objects.requireNonNull(event.getButton().getId())) {
            case "yes" -> {
                offerWizard.getOffer().setHaveAdditionals(true);
                event.reply(
                        "**Welche zus\u00e4tzlichen Informationen sollen angehangen werden?**\n" +
                                "Beispiel: *mindestens 18 Jahre*, *Erfahrungen in XY*... "
                ).queue();
                offerWizard.setStep(ADDITIONALS);
            }
            case "no" -> {
                offerWizard.getOffer().setHaveAdditionals(false);
                event.reply("Ist dein Angebot f\u00fcr bestimmt Zeit ge\u00f6ffnet und du sammelst alle Bewerbungen oder m\u00f6chtest du so schnell wie m\u00f6glich anfangen?")
                        .addActionRow(
                                Button.success(BUTTONCOLLECT, BUTTONSAMMELN),
                                Button.danger(BUTTONINSTANT, BUTTONSCHNELL)
                        ).queue();
                offerWizard.setStep(COLLECT);
            }
        }
    }

    public static void setAdditionalsTriggerCollect(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setAdditionalsTriggerCollect by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setAdditionals(message);

        event.getMessage().reply("**Ist dein Angebot f\u00fcr eine bestimmt Zeit ge\u00f6ffnet und du sammelst vorerst alle Bewerbungen**\n\toder\n**M\u00f6chtest du so schnell wie m\u00f6glich anfangen?**")
                .setActionRow(
                        Button.success(BUTTONCOLLECT, BUTTONSAMMELN),
                        Button.danger(BUTTONINSTANT, BUTTONSCHNELL)
                ).queue();
        offerWizard.setStep(COLLECT);

    }

    public static void setCollectTriggerCollectionDateOrFinish(ButtonInteractionEvent event, OfferWizard offerWizard) {
        logInfo("setCollectTriggerCollectionDateOrFinish by: " + event.getUser());
        switch (Objects.requireNonNull(event.getButton().getId())) {
            case BUTTONCOLLECT -> {
                offerWizard.getOffer().setCollect(true);
                event.reply("**Bis wann soll das Angebot ge\u00f6ffnet bleiben / Bis wann m\u00f6chtest du die Anmeldungen sammeln?**\n" +
                        "Beispiel: *2. Juli 2022*, *27.12.2022, 3.Mai*"
                ).queue();
                offerWizard.setStep(COLLECTDATE);
            }
            case BUTTONINSTANT -> {
                offerWizard.getOffer().setCollect(false);
                triggerFinish(event.getMessage(), offerWizard);
            }
        }
    }

    public static void setCollectionDateTriggerFinish(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setCollectionDateTriggerFinish by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setCollectionDate(message);

        triggerFinish(event.getMessage(), offerWizard);
    }

    public static void triggerFinish(Message message, OfferWizard offerWizard) {
        LogWrapper.logInfo("triggerFinish by: " + message.getAuthor());
        offerWizard.setStep(CLOSE);

        LogWrapper.logInfo("persist offer in Database..");
        OfferComp oc = Runner.getCTX().getBean(OfferComp.class);
        Offer offer = offerWizard.getOffer();
        oc.saveOffer(offer);
        LogWrapper.logInfo("persisted!");

        List<String> offerParts = StringSplitter.splitString(offer.toString(), 1900, 100);

        message.getChannel().sendMessage("Dies ist dein Angebot: \n\n").queue();
        for (String offerPart : offerParts) {
            message.getChannel().sendMessage(CODEMARKER + offerPart + CODEMARKER).queue();
        }
        message.getChannel().sendMessage("\n\n\n\n\n" +
                "Dieses Angebot wurde an die Moderator*innen geschickt. Wenn du etwas daran \u00e4ndern m\u00f6chtest, wende dich gern an sie.\n" +
                "Danke, dass du den Angebots-Wizard genutzt hast!\n\n" +
                "Deine Angebots-ID: " + offer.getOfferId()
        ).queue();
        message.getChannel().sendMessage("https://tenor.com/view/pepe-pepe-the-frog-wizard-gif-7939266").queue();

        //to post

        sendMessageInChannel(Server.MITNERMENGEFANTASIE, Channel.TOPOST, offerWizard, message);
        archivePost(message, offerWizard);
    }

    private static void sendMessageInChannel(Server server, Channel channel, OfferWizard offerWizard, Message message){
        List<String> offerParts = StringSplitter.splitString(offerWizard.getOffer().toString(), 1900, 100);
        Offer offer = offerWizard.getOffer();
        try {
            TextChannel toPostChannel = message.getJDA().getGuildById(server.getIdAsLong()).getTextChannelById(channel.getIdAsLong());
            if (toPostChannel != null) {
                toPostChannel.sendMessage(OFFERIDTEXT + offer.getOfferId()  + "\n").queue();
                for (String offerPart : offerParts) {
                    toPostChannel.sendMessage(CODEMARKER  + offerPart + CODEMARKER).queue();
                }
            } else {
                //contact Mods
                TextChannel modChannel = message.getJDA().getGuildById(Server.MITNERMENGEFANTASIE.getIdAsLong()).getTextChannelById(Channel.MODAREA.getIdAsLong());
                if(modChannel != null) {
                    modChannel.sendMessage(
                            "TO-POST ist nicht erreichbar.\nNeues angebot ID: " + offer.getOfferId()
                    ).queue();
                }
            }
        } catch (Exception e) {
            logSevere(e.getMessage());
        }
    }

    public static void setWTitleTriggerWDesc(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setWTitleTriggerWDesc by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setTitle(message);

        event.getMessage().reply("Dein Workshop tr\u00e4gt den Titel: " + message + BIGLINEBREAK +
                        "**Fasse doch bitte einmal zusammen, worum es in deinem Workshop gehen soll:**\n" +
                        "*Sei gern etwas ausführlicher, damit die anderen wissen, worum es geht.*")
                .queue();

        offerWizard.setStep(WDESC);
    }

    public static void setWDescTriggerWSize(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setWDescTriggerWSize by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setDescript(message);

        event.getMessage().reply("""
                        Da hätte ich direkt lust mich einzuschreiben, um etwas zu lernen!

                        **Wie viele Personen d\u00fcrfen denn an dem Workshop teilnehmen?**
                        "Beispiel: *5*, *2-9*, *3*" """)
                .queue();

        offerWizard.setStep(WSIZE);
    }

    public static void setWSizeTriggerFixedDate(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setWSizeTriggerFixedDate by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setSize(message);
        event.getMessage().reply(
                        message + " Personen könnten an deinem Workshop teilnehmen.\n\n" +
                                "**Findet das Workshop an einem festen Termin statt?**\n\n" +
                                "Klicke hier auch auf \"Ja\", wenn dir bereits ein Datum vor Augen steht.\n" +
                                "Beispiel: *26. Mai* oder *jeden 2. Donnerstag im Monat*"

                )
                .setActionRow(
                        Button.success("yes", "Ja"),
                        Button.danger("no", "Nein")
                )
                .queue();
        offerWizard.setStep(WFIXDATE);
    }

    public static void setWFixDateTriggerWDateOrKnow(ButtonInteractionEvent event, OfferWizard offerWizard) {
        logInfo("setWFixDateTriggerWDateOrKnow by: " + event.getUser());
        switch (Objects.requireNonNull(event.getButton().getId())) {
            case "yes" -> {
                offerWizard.getOffer().setFixedDate(true);
                event.reply(
                        """
                                Du möchtest deinen Workshop an einem festen Tag stattfinden lassen.

                                **Gib nun das Datum ein, an dem der Workshop stattfinden soll.**
                                Beispiel: *2. Juli 2023  20:30 Uhr*, *27.12.2023*, *3.Mai*, *hauptsache nach 16 Uhr*"""
                ).queue();
                offerWizard.setStep(WDATE);
            }
            case "no" -> {
                offerWizard.getOffer().setFixedDate(false);
                event.reply(
                        "Erwartest du irgendwelche Vorkentnisse?\n" +
                                "Beispiel: Schoneinmal als Spieler gespielt"
                ).queue();
                offerWizard.setStep(WKNOWLEDGE);
            }
        }
    }

    public static void setWDateTriggerWKnow(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setWDateTriggerWKnow by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setPlayDate(message);
        event.getMessage().reply(
                "**Erwartest du irgendwelche Vorkentnisse?**\n" +
                        "Beispiel: Schoneinmal als Spieler gespielt"
        ).queue();
        offerWizard.setStep(WKNOWLEDGE);
    }

    public static void setWKnowTriggerWLength(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setWKnowTriggerWHowJoin by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setAdditionals(message);

        event.getMessage().reply( "**Was denkst du, wie lang wird der Workshop in etwa gehen?**\n" +
                        "Beispiel: *3-4 Abende \u00e1 4 Stunden*, *in Summe etwa 90 Stunden*, *Workshop an einem Abend 3-6 Stunden*...")
                .queue();

        offerWizard.setStep(WLENGTH);
    }

    public static void setWHowJoinTriggerWCollect(MessageReceivedEvent event, OfferWizard offerWizard){
        logInfo("setWLengthTriggerWHowJoin by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setLength(message);
        logInfo("setWHowJoinTriggerWCollect by: " + event.getAuthor());

        event.getMessage().reply("**Ist dein Workshop f\u00fcr eine bestimmt Zeit ge\u00f6ffnet und du sammelst vorerst alle Bewerbungen**\n\toder\n**M\u00f6chtest du so schnell wie m\u00f6glich anfangen?**")
                .setActionRow(
                        Button.success(BUTTONCOLLECT, BUTTONSAMMELN),
                        Button.danger(BUTTONINSTANT, BUTTONSCHNELL)
                ).queue();
        offerWizard.setStep(WCOLLECT);
    }

    public static void setWCollectTriggerWDateOrFinish(ButtonInteractionEvent event, OfferWizard offerWizard) {
        logInfo("setWCollectTriggerWDateOrFinish by: " + event.getUser());
        switch (Objects.requireNonNull(event.getButton().getId())) {
            case BUTTONCOLLECT -> {
                offerWizard.getOffer().setCollect(true);
                event.reply("**Bis wann soll der Workshop ge\u00f6ffnet bleiben / Bis wann m\u00f6chtest du die Anmeldungen sammeln?**\n" +
                        "Beispiel: *2. Juli 2022*, *27.12.2022, 3.Mai*"
                ).queue();
                offerWizard.setStep(WCOLLECTDATE);
            }
            case BUTTONINSTANT -> {
                offerWizard.getOffer().setCollect(false);
                triggerWFinish(event.getMessage(), offerWizard);
            }
        }
    }



    private static void triggerWFinish(Message message, OfferWizard offerWizard) {
        LogWrapper.logInfo("triggerWFinish by: " + message.getAuthor());
        offerWizard.setStep(WCLOSE);

        LogWrapper.logInfo("persist workshop in Database..");
        OfferComp oc = Runner.getCTX().getBean(OfferComp.class);
        Offer offer = offerWizard.getOffer();
        oc.saveOffer(offer);
        LogWrapper.logInfo("persisted!");

        List<String> offerParts = StringSplitter.splitString(offer.toString(), 1900, 100);

        message.getChannel().sendMessage("Dies ist dein Workshop: \n\n").queue();
        for (String offerPart : offerParts) {
            message.getChannel().sendMessage(CODEMARKER + offerPart + CODEMARKER).queue();
        }
        message.getChannel().sendMessage("\n\n\n\n\n" +
                "Dieses Workshop-Angebot wurde an die Moderator*innen geschickt. Wenn du etwas daran \u00e4ndern m\u00f6chtest, wende dich gern an sie.\n" +
                "Danke, dass du den Angebots-Wizard genutzt hast!\n\n" +
                "Deine Angebots-ID: " + offer.getOfferId()
        ).queue();
        message.getChannel().sendMessage("https://tenor.com/view/pepe-pepe-the-frog-wizard-gif-7939266").queue();

        //to post
        sendMessageInChannel(Server.MITNERMENGEFANTASIE, Channel.TOPOSTWORKSHOP, offerWizard, message);
    }

    public static void setWCollecDateTriggerWFinish(MessageReceivedEvent event, OfferWizard offerWizard) {
        logInfo("setWCollecDateTriggerWFinish by: " + event.getAuthor());
        String message = event.getMessage().getContentRaw();
        offerWizard.getOffer().setCollectionDate(message);

        triggerWFinish(event.getMessage(), offerWizard);
    }



    private static void archivePost(Message message, OfferWizard offerWizard){
        Offer offer = offerWizard.getOffer();
        List<String> offerParts = StringSplitter.splitString(offer.toString(), 1900, 100);

        try {
            TextChannel archive = message.getJDA().getGuildById(Server.MITNERMENGEFANTASIE.getIdAsLong()).getTextChannelById(Channel.ARCHIVE.getIdAsLong());
            if (!archive.getName().isEmpty()) {
                message.getJDA().getGuildById(Server.MITNERMENGEFANTASIE.getIdAsLong()).getTextChannelById(Channel.ARCHIVE.getIdAsLong()).sendMessage(OFFERIDTEXT + offer.getOfferId()  + "\n").queue();
                for (String offerPart : offerParts) {
                    message.getJDA().getGuildById(Server.MITNERMENGEFANTASIE.getIdAsLong()).getTextChannelById(Channel.ARCHIVE.getIdAsLong()).sendMessage( CODEMARKER  + offerPart + CODEMARKER).queue();
                }
            } else {
                message.getJDA().getGuildById(Server.MITNERMENGEFANTASIE.getIdAsLong()).getTextChannelById(Channel.MODAREA.getIdAsLong()).sendMessage(
                        "Archiv ist nicht erreichbar.\nNeues angebot (ID:" + offer.getOfferId() + " ) konnte nicht ins Archiviert werden"
                ).queue();
            }
        } catch (Exception e) {
            logSevere(e.getMessage());
        }
    }
    private static void logInfo(String message) {
        LogWrapper.logInfo(message);
    }
    private static void logSevere(String message) {
        LogWrapper.logSevere(message);
    }
}