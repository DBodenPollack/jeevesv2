package de.devngs.jeeves.bot.exceptions;

public class BotCommandNotFoundException extends Exception {
    public BotCommandNotFoundException(String errorMessage){
        super(errorMessage);
    }
    public BotCommandNotFoundException(String errorMessage, Throwable err){
        super(errorMessage, err);
    }
}
