package de.devngs.jeeves.bot.commands;

import de.devngs.jeeves.bot.additionals.handler.DiceHandler;
import de.devngs.jeeves.utils.LogWrapper;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RollCommand extends ListenerAdapter {
    @Override
    public void onSlashCommandInteraction(final SlashCommandInteractionEvent event)
    {
        if (!event.getName().equals("roll")) return;
        LogWrapper.logInfo("start roll commmand by: " + event.getUser());
        List<Integer> ergs = new ArrayList<>();

        int number = Integer.parseInt(Objects.requireNonNull(event.getOption("number")).getAsString());
        int dice = Integer.parseInt(Objects.requireNonNull(event.getOption("dicesize")).getAsString());

        Integer singleMod = (event.getOption("singlemod") != null) ?
                Integer.parseInt(Objects.requireNonNull(event.getOption("singlemod")).getAsString()):
                null;
        Integer eachMod = (event.getOption("eachmod") != null) ?
                Integer.parseInt(Objects.requireNonNull(event.getOption("eachmod")).getAsString()):
                null;
        String secString = (event.getOption("secret") != null) ?
                Objects.requireNonNull(event.getOption("secret")).getAsString().substring(0, 1).toLowerCase():
                null;

        boolean secret = Objects.equals(secString, "y");

        for (int i = 0; i < number; i++){
            Integer erg = DiceHandler.rollDice(1, dice);
            if(eachMod != null)  erg += eachMod;
            ergs.add(erg);
        }
        int erg = ( ergs.stream().mapToInt(integer -> integer).sum() );

        if(singleMod != null ) erg += singleMod;

        String singleModText = (singleMod == null ) ? "" : " ,"+ singleMod;
        String eachModText = (eachMod == null ) ?  "": " zus\u00e4tzlich wurde auf jeden Wurf der Wert " + eachMod + " dazu addiert. ";

        String rollSum = "";
        if(ergs.toString().length() < 1500){
            rollSum =
                "\n" +
                "W\u00fcrfelergebnisse\n" +
                "\t" +  ergs.toString().replaceAll("[]\\[]", "") + singleModText + "\n";
        }

        String message = "Es wurde " + number + " mal ein D" + dice + " gew\u00fcrfelt" + eachModText + "\n"+
                rollSum +
                "Ergebnis:\n" +
                "\t" + erg;

        event.reply(message).setEphemeral(secret).queue();

    }
}
