package de.devngs.jeeves.bot.commands;

import de.devngs.jeeves.utils.LogWrapper;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class JeevesCommand extends ListenerAdapter {

    private static final String COMMAND_NAME = "jeeves";
    private static final String HELP_RESOURCE_PATH = "classpath:META-INF/JeevesHelp";

    @Override
    public void onSlashCommandInteraction(@NotNull final SlashCommandInteractionEvent event) {
        if (event.getName().equals(COMMAND_NAME)) {
            LogWrapper.logInfo("Start Jeeves help command by: " + event.getUser());
            String helpMessage = loadHelpTemplate();
            if (helpMessage == null) {
                helpMessage = "Sorry, I can't provide help at the moment.";
            }
            sendHelpResponse(event, helpMessage);
        }
    }

    private String loadHelpTemplate() {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        try {
            return resourceLoader.getResource(HELP_RESOURCE_PATH).getContentAsString(StandardCharsets.UTF_8);
        } catch (IOException e) {
            LogWrapper.logWarning("JeevesHelp template could not be found. " + e.getMessage());
            return null;
        }
    }

    private void sendHelpResponse(SlashCommandInteractionEvent event, String helpMessage) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Who or what is Jeeves?");
        eb.setDescription(helpMessage);
        event.replyEmbeds(eb.build()).setEphemeral(true).queue();
    }
}
