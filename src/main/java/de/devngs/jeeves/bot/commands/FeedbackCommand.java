package de.devngs.jeeves.bot.commands;

import de.devngs.jeeves.Runner;
import de.devngs.jeeves.bot.additionals.enums.Channel;
import de.devngs.jeeves.bot.additionals.enums.Role;
import de.devngs.jeeves.utils.LogWrapper;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class FeedbackCommand extends ListenerAdapter {
    @Override
    public void onSlashCommandInteraction(@NotNull final SlashCommandInteractionEvent event) {
        if (event.getName().equals("feedback")){
            LogWrapper.logInfo("start feedback command by: " + event.getUser());

            String feedback = Objects.requireNonNull(event.getOption("text")).getAsString();
            TextChannel tc = Objects.requireNonNull(event.getJDA().getGuildById(Runner.getServer().getIdAsLong())).getTextChannelById(Channel.MODSUPPORT.getId());
            assert tc != null;
            tc.sendMessage(
                    "────────────────────────────\n" +
                    "***Feedback von <@" + event.getUser().getId() + ">***\n\n\n"+
                    "**Content:**\n" +
                    feedback + "\n" +
                    "────────────────────────────"
            ).queue();
            if (event.getChannel().getType() == ChannelType.PRIVATE){
                event.reply("Feedback wurde an die " + Role.MODERATOR.getName() + "nen weitergereicht!").queue();
            } else {
                event.reply("Feedback wurde an die <@&" + Role.MODERATOR.getId() + ">nen weitergereicht!").setEphemeral(true).queue();
            }

        }
    }

}
