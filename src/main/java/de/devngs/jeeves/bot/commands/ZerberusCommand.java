package de.devngs.jeeves.bot.commands;

import de.devngs.jeeves.Runner;
import de.devngs.jeeves.bot.additionals.Helper;
import de.devngs.jeeves.bot.additionals.enums.DcUser;
import de.devngs.jeeves.bot.additionals.enums.Role;
import de.devngs.jeeves.bot.additionals.enums.Server;
import de.devngs.jeeves.bot.additionals.handler.OfferHandler;
import de.devngs.jeeves.bot.additionals.handler.SurveyHandler;
import de.devngs.jeeves.bot.additionals.handler.ZerberusAdditionalsHandler;
import de.devngs.jeeves.utils.LogWrapper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ZerberusCommand extends ListenerAdapter {

    @Override
    public void onSlashCommandInteraction(final SlashCommandInteractionEvent event) {
        if (!event.getName().equals("zerberus")) return;
        LogWrapper.logInfo("start zerberus command by: " + event.getUser());

        Guild serv = event.getGuild();
        if (serv == null && event.getChannel().getType() == ChannelType.PRIVATE) {
            event.reply("Dieser Befehl funktioniert nicht im Privaten Chat.").queue();
            return;
        }
        assert serv != null;
        //for Development - Add role Role.DEVELOPER.getId() here.
        if (!(event.getMember() == event.getGuild().getOwner() ||
                (Runner.getServer() == Server.DEMOCHANNEL && event.getMember().getId().equals(DcUser.DANIEL.getId())) ||
                (Helper.isInRoles(event.getMember(), List.of(Role.MODERATOR.getId(), Role.OWNER.getId()))))) {

            event.reply("Dieser Befehl ist ausschließlich für Moderator/innen und Dave.").setEphemeral(true).queue();
            return;
        }
        triggerEvent(event);
    }


    private void triggerEvent(SlashCommandInteractionEvent event){
        try {
            String mode = event.getOption("mode").getAsString();
            switch (mode) {
                case "LIST" -> ZerberusAdditionalsHandler.performListCommand(event);
                //survey
                case "CREATE" -> SurveyHandler.performCreateCommand(event);
                case "START" -> SurveyHandler.startSurvey(event);
                case "STOP" -> SurveyHandler.stopSurvey(event);
                case "STATE" -> SurveyHandler.getSurveyState(event);
                case "GETA" -> SurveyHandler.performGetAllCommand(event);
                case "DELETE" -> SurveyHandler.performDeleteSurvey(event);
                //offers
                case "GETALLOFFERS" -> OfferHandler.getAllOffers(event);
                case "GETOFFERBYID" ->
                        OfferHandler.getOfferByID(event, event.getOption("id").getAsString());
                case "GETOPENOFFERS" -> OfferHandler.getOpenOffers(event);
                case "POSTALLOFFERS" -> OfferHandler.postAllOffers(event);
                case "POSTOFFERBYID" ->
                        OfferHandler.postOfferByID(event, event.getOption("id").getAsString());
                case "EDITOFFERBYID" ->
                        OfferHandler.editOfferByID(event, event.getOption("id").getAsString(), event.getOption("offer").getAsString());
                case "DELETEOFFERBYID" ->
                        OfferHandler.deleteOfferByID(event, event.getOption("id").getAsString());

                default -> event.reply("Wrong mode").queue();
            }
        } catch (NullPointerException exception) {
            event.reply("Parameter is missing").queue();
        }
    }

    @Override
    public void onButtonInteraction(@NotNull final ButtonInteractionEvent event) {
        SurveyHandler.setUserState(event);
    }
}
