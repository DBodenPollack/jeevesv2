package de.devngs.jeeves.bot.commands;

import de.devngs.jeeves.bot.additionals.handler.CalcHandler;
import de.devngs.jeeves.utils.LogWrapper;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import org.jetbrains.annotations.NotNull;

public class CalcCommand extends ListenerAdapter {

    @Override
    public void onSlashCommandInteraction(@NotNull final SlashCommandInteractionEvent event) {
        if (!event.getName().equals("calc")) return;
        LogWrapper.logInfo("start calc command by: " + event.getUser());
        OptionMapping math = event.getOption("task");
        if (math == null ){
            event.reply("Aufgabe darf nicht leer sein. Bitte wert \"task\" füllen").queue();
            return;
        }

        String erg = CalcHandler.calc(math.getAsString());
        event.reply(erg).queue();

    }
}
