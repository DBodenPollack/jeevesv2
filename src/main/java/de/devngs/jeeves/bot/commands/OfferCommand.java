package de.devngs.jeeves.bot.commands;

import de.devngs.jeeves.bot.additionals.Helper;
import de.devngs.jeeves.bot.additionals.enums.DcUser;
import de.devngs.jeeves.bot.additionals.enums.Role;
import de.devngs.jeeves.bot.additionals.handler.OfferCommandHandler;
import de.devngs.jeeves.bot.additionals.objects.OfferWizard;
import de.devngs.jeeves.database.mysql.entity.Offer;
import de.devngs.jeeves.utils.LogWrapper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static de.devngs.jeeves.bot.additionals.enums.Step.CLOSE;
import static de.devngs.jeeves.bot.additionals.enums.Step.GAMETYPE;

public class OfferCommand extends ListenerAdapter {
    private final List<OfferWizard> offerWizardList = new ArrayList<>();

    @Override
    public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {

        if (event.getName().equals("offer")) {
            LogWrapper.logInfo("start offer Command");
            User user = event.getUser();
            Guild guild = event.getGuild();

            if (guild == null) {
                event.reply("Dieser Befehl funktioniert nur aus dem Server heraus :(!").setEphemeral(true).queue();
                return;
            }

            Member member = guild.getMemberById(user.getId());
            if (member == null) return;
            if (Helper.isInRoles(member, List.of(Role.GAMEMASTER.getId(), Role.PAUSED_GAMEMASTER.getId(), Role.PLANNED_GAMEMASTER.getId(), DcUser.KOKU.getId())) ||
                    member.getId().equals(guild.getOwnerId()) || member.getId().equals(DcUser.DANIEL.getId())) {

                OfferWizard offerWizard = new OfferWizard();
                offerWizard.setUser(user);
                offerWizard.setStep(GAMETYPE);
                offerWizard.setOffer(new Offer());
                offerWizard.getOffer().setGameMaster(user.getIdLong());

                user.openPrivateChannel().queue(channel -> channel.sendMessage(
                                """
                                        Hinweis: Dieser Prozess kann jederzeit unterbrochen werden. Schreibe einfach 'STOP' [ohne '] in den Chat!

                                        Was m\u00f6chtest du leiten?""")
                        .setActionRow(Button.success("campaign", "Kampagne"), Button.success("oneshot", "OneShot"), Button.primary("workshop", "Workshop")).queue());

                event.reply("Schau mal in den Privaten Chat").setEphemeral(true).queue();
                offerWizard.setChannelType(ChannelType.PRIVATE);
                offerWizardList.add(offerWizard);
            } else {
                event.reply("Leider fehlt dir die notwendige Rolle :(").setEphemeral(true).queue();
            }
        }
    }

    @Override
    public void onButtonInteraction(@NotNull ButtonInteractionEvent event) {
        LogWrapper.logInfo("Button interaction offer Command by: " + event.getUser());
        OfferWizard offerWizard = null;
        for (OfferWizard search : offerWizardList) {
            if (search.getChannelType() == event.getChannel().getType() && search.getUser() == event.getUser()) {
                offerWizard = search;
            }
        }
        if (offerWizard == null) return;
        int i = offerWizardList.indexOf(offerWizard);

        switch (offerWizard.getStep()) {
            case GAMETYPE:
                OfferCommandHandler.setGameTypeTriggerSystem(event, offerWizard);
                break;
            case FIXEDDATE:
                OfferCommandHandler.setFixedDateAndTriggerDateOrHow(event, offerWizard);
                break;
            case WHO:
                OfferCommandHandler.setWhoTriggerHaveAdditionals(event, offerWizard);
                break;
            case HAVEADDITIONALS:
                OfferCommandHandler.setHaveAdditionalsTriggerAdditionalsOrCollect(event, offerWizard);
                break;
            case COLLECT:
                OfferCommandHandler.setCollectTriggerCollectionDateOrFinish(event, offerWizard);
                break;
            case WFIXDATE:
                OfferCommandHandler.setWFixDateTriggerWDateOrKnow(event, offerWizard);
                break;
            case WCOLLECT:
                OfferCommandHandler.setWCollectTriggerWDateOrFinish(event, offerWizard);
                break;
            default:
                return;
        }
        if (offerWizard.getStep() != CLOSE) {
            offerWizardList.set(i, offerWizard);
        } else {
            offerWizardList.remove(i);
        }
    }

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        OfferWizard offerWizard = null;
        for (OfferWizard search : offerWizardList) {
            if (search.getChannelType() == event.getChannel().getType() && search.getUser() == event.getAuthor()) {
                offerWizard = search;
            }
        }
        if (offerWizard == null) return;
        LogWrapper.logInfo("messace recive by interaction offer Command by :" + event.getAuthor());
        int i = offerWizardList.indexOf(offerWizard);

        if (event.getMessage().getContentRaw().equalsIgnoreCase("STOP")) {
            event.getMessage().reply("Der Prozess zum erstellen eines Spielgesuchs wird beendet!").queue();
            offerWizard.setStep(CLOSE);
            offerWizardList.remove(offerWizard);
            return;
        }

        switch (offerWizard.getStep()) {
            case SYSTEM:
                OfferCommandHandler.setSystemTriggerSize(event, offerWizard);
                break;
            case SIZE:
                OfferCommandHandler.setSizeTriggerFixedDate(event, offerWizard);
                break;
            case DATE:
                OfferCommandHandler.setDateTriggerHow(event, offerWizard);
                break;
            case HOW:
                OfferCommandHandler.setHowTriggerLength(event, offerWizard);
                break;
            case LENGTH:
                OfferCommandHandler.setLengthTriggerSeriousness(event, offerWizard);
                break;
            case SERIOUSNESS:
                OfferCommandHandler.setSeriousnessTriggerTitel(event, offerWizard);
                break;
            case TITLE:
                OfferCommandHandler.setTitleTriggerDescription(event, offerWizard);
                break;
            case DESCRIPTION:
                OfferCommandHandler.setDescriptionTriggerTrigger(event, offerWizard);
                break;
            case TRIGGER:
                OfferCommandHandler.setTriggerTriggerWho(event, offerWizard);
                break;
            case ADDITIONALS:
                OfferCommandHandler.setAdditionalsTriggerCollect(event, offerWizard);
                break;
            case COLLECTDATE:
                OfferCommandHandler.setCollectionDateTriggerFinish(event, offerWizard);
                break;
            case WTITLE:
                OfferCommandHandler.setWTitleTriggerWDesc(event, offerWizard);
                break;
            case WDESC:
                OfferCommandHandler.setWDescTriggerWSize(event, offerWizard);
                break;
            case WSIZE:
                OfferCommandHandler.setWSizeTriggerFixedDate(event, offerWizard);
                break;
            case WDATE:
                OfferCommandHandler.setWDateTriggerWKnow(event, offerWizard);
                break;
            case WKNOWLEDGE:
                OfferCommandHandler.setWKnowTriggerWLength(event, offerWizard);
                break;
            case WLENGTH:
                OfferCommandHandler.setWHowJoinTriggerWCollect(event, offerWizard);
                break;
            case WCOLLECTDATE:
                OfferCommandHandler.setWCollecDateTriggerWFinish(event, offerWizard);
                break;
            case INITIAL, CLOSE:
            default:
                return;
        }

        if (offerWizard.getStep() != CLOSE) {
            offerWizardList.set(i, offerWizard);
        } else {
            offerWizardList.remove(i);
        }

    }

}


