package de.devngs.jeeves.database.mysql.repository;

import de.devngs.jeeves.database.mysql.entity.OfferMessages;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface OfferMessageRepository extends JpaRepository<OfferMessages, UUID> {
    Optional<OfferMessages> findOfferMessagesByOfferId(UUID id);
}
