package de.devngs.jeeves.database.mysql.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Transactional
public class DefaultTexts {
    @Id
    String identName;

}
