package de.devngs.jeeves.database.mysql.service.implementations;


import de.devngs.jeeves.database.mysql.entity.SurveyVote;
import de.devngs.jeeves.database.mysql.repository.SurveyVoteRepository;
import de.devngs.jeeves.database.mysql.service.interfaces.SurveyVoteComp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Transactional
@Component
public class SurveyVoteCompImpl implements SurveyVoteComp {

    @Autowired
    private SurveyVoteRepository surveyVoteRepository;

    @Override
    public List<SurveyVote> findAll() {
        return surveyVoteRepository.findAll();
    }

    @Override
    public SurveyVote saveSurveyVote(SurveyVote surveyVote) {
        return surveyVoteRepository.save(surveyVote);
    }

    @Override
    public void deleteSurveyVote(SurveyVote surveyVote) {
        surveyVoteRepository.delete(surveyVote);
    }

    @Override
    public List<SurveyVote> findBySurveyId(UUID surveyId) {
        List<SurveyVote> tmp =  surveyVoteRepository.findAll();
        List<SurveyVote> ret = new ArrayList<>();
        for(SurveyVote sv : tmp){
            if (sv.getSurveyId().equals(surveyId)){
                ret.add(sv);
            }
        }
        return ret;
    }

    @Override
    public void deleteAllVotesBySurveyId(UUID surveyId) {
        surveyVoteRepository.deleteAllBySurveyId(surveyId);
    }
}
