package de.devngs.jeeves.database.mysql.entity;

import lombok.*;
import jakarta.persistence.*;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@IdClass(SurveyVoteKeys.class)
public class SurveyDelProb {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID surveyId;
    @Id
    private Long userId;

}
