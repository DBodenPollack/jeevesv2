package de.devngs.jeeves.database.mysql.service.interfaces;

import de.devngs.jeeves.database.mysql.entity.SurveyVote;

import java.util.List;
import java.util.UUID;

public interface SurveyVoteComp {
    List<SurveyVote> findAll();
    SurveyVote saveSurveyVote(SurveyVote surveyVote);
    void deleteSurveyVote(SurveyVote surveyVote);

    List<SurveyVote> findBySurveyId(UUID surveyId);

    void deleteAllVotesBySurveyId(UUID surveyId);
}