package de.devngs.jeeves.database.mysql.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.UuidGenerator;


import java.util.List;
import java.util.UUID;

@NoArgsConstructor @AllArgsConstructor @Getter @Setter
@Entity
public class Survey {
    @Id
    @GeneratedValue(generator = "UUID")
    @UuidGenerator
    @Column(columnDefinition = "BINARY(16)", name = "id", updatable = false, nullable = false)
    private UUID id;
    @Column(length=1000)
    private String survey;
    private String options;
    private Long targetRole;
    private boolean active;

    public Survey(String survey, String options, Long targetRole){
        this.id = UUID.randomUUID();
        this.survey = survey;
        this.options = options;
        this.targetRole = targetRole;
        this.active = false;
    }

    @Override
    public String toString(){
        return "ID: " + this.id + "\t\t\tSurvey: " + this.survey + "\t\t\tOptions: " + this.options;
    }
    public String toStringWithBreaks(){
        StringBuilder sb = new StringBuilder();
        sb.append("**------------------------------------**\n");
        sb.append("**ID:** ").append(this.id).append("\n");
        sb.append("**Survey:** ").append(this.survey).append("\n");
        sb.append("**Options:** ");
        for( String opt : getSplittedOptions()){
            sb.append(opt).append("\t\t");
        }
        sb.append("\n");
        if (this.active) {
            sb.append("\n");
            sb.append("**ACTIVE!**");
            sb.append("\n");
        }
        sb.append("**------------------------------------**\n");

        return  sb.toString();
    }

    public List<String> getSplittedOptions(){
        return List.of(this.options.split("\\|"));
    }

}
