package de.devngs.jeeves.database.mysql.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.devngs.jeeves.bot.additionals.enums.GameType;
import de.devngs.jeeves.bot.additionals.enums.Role;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.*;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * *Spielleiter/in:**
 * <p>
 * *Spielsystem:**
 * *Gruppengröße:**
 * <p>
 * (Optional, bei festem Datum: **Wann wird gespielt:**)
 * *Wie wird gespielt:**
 * <p>
 * *Wie lange geht der OneShot/die Kampagne:**
 * <p>
 * *Ernsthaftigkeit:**
 * <p>
 * *Was wird das Setting:** *"[TITEL]"*
 * [Beschreibung]
 * <p>
 * *Triggerwarnung:** bspw. Cthulhu -> Kontakt mit psychischen Krankheiten, Suizid, ...
 * <p>
 * *Wie kann ich teilnehmen:**
 * *Wer darf teilnehmen:**
 * <p>
 * *Angebot geöffnet bis:**
 * <p>
 * ---------------------------------------------------------------------------------------------------------------------
 * <p>
 * *Workshopleiter/in:**
 * <p>
 * *Workshopinhalte:** *"[TITEL]"*
 * [Beschreibung]
 * <p>
 * *Gruppengröße:**
 * (Optional, bei festem Datum: **Wann findet der Workshop statt:**)
 * <p>
 * *Benötigte Vorkenntnisse:** ==> additionals true => Additionals
 * <p>
 * *Wie lange geht der Workshop:**
 * <p>
 * *Wie kann ich teilnehmen:** PN an {Workshopleiter/in}
 * *Wer darf teilnehmen:** @rollen markieren (@Workshopinteressent/in)
 * <p>
 * *Angebot geöffnet bis:** [festes Datum / Voll -> First come, first serve!]
 */


//Idea: Implement the Strings with XML Templates and parser
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Transactional
public class Offer {
    private static final String LB = "\r";
    private static final String INDENT = "\u3000";
    private static final String FULL = "Voll - means: first come, first served!";
    private static final String TPGAMETYPE = "{{GameType}}";
    private static final String TPGAMEMASTER = "{{GameMaster}}";
    private static final String TPSYSTEM = "{{System}}";
    private static final String TPSIZE = "{{Size}}";
    private static final String TPPLAYDATE = "{{When}}";
    private static final String WSPLAYDATETITLE = "\r**Wann findet der Workshop statt:** ";
    private static final String PLAYDATETITLE = "\r**Wann wird gespielt:** ";
    private static final String TPHOW = "{{How}}";
    private static final String TPLENGHT = "{{Length}}";
    private static final String TPSERIOUSNESS = "{{Seriousness}}";
    private static final String TPTITLE = "{{Title}}";
    private static final String TPDESC = "{{Description}}";
    private static final String TPTRIGGER = "{{Trigger}}";
    private static final String TPHOWJOIN = "{{HowJoin}}";
    private static final String TPWHO = "{{Who}}";
    private static final String TPADDITIONALS = "{{Additionals}}";
    private static final String WSADDITIONALSTITLE = "\r**Benötigte Vorkenntnisse:** ";
    private static final String ADDITIONALSTITLE = "\r**Zus\u00e4tzliche Teilnahmeinformationen:**\r";
    private static final String TPCOLLECTDATE = "{{OpenDate}}";
    @Id
    @GeneratedValue(generator = "UUID")
    @UuidGenerator
    @Column(columnDefinition = "BINARY(16)", name = "id", updatable = false, nullable = false)
    private UUID offerId;
    private boolean posted;
    private GameType gameType;
    private Long gameMaster = 0L;
    @Column(columnDefinition = "text")
    private String gameSystem = "";
    @Column(columnDefinition = "text")
    private String size = "";
    private boolean fixedDate = false;
    @Column(columnDefinition = "text")
    private String playDate = "";
    @Column(columnDefinition = "text")
    private String howPlay = "";
    @Column(columnDefinition = "text")
    private String length = "";
    @Column(columnDefinition = "text")
    private String seriousness = "";
    private String title = "";
    @Column(columnDefinition = "text")
    private String descript = "";
    @Column(columnDefinition = "text")
    private String triggerList = "";
    @Column(columnDefinition = "text")
    private String howJoin = "";
    private Role whoCanJoin;
    private boolean haveAdditionals;
    @Column(columnDefinition = "text")
    private String additionals = "";
    private boolean collect;
    @Column(columnDefinition = "text")
    private String collectionDate = "";

    public static Offer jsonToOffer(String offerJson) {
        Gson g = new Gson();
        return g.fromJson(offerJson, Offer.class);
    }

    public String toAdminString() {
        return offerToJson();
    }

    public String offerToJson() {
        Gson g = new GsonBuilder().setPrettyPrinting().create();
        return g.toJson(this);
    }

    @Override
    public String toString() {
        try {
            switch (this.gameType) {
                case WORKSHOP -> {
                    String workshopTemplate = new DefaultResourceLoader().getResource("classpath:META-INF/WorkshopTemplate").getContentAsString(StandardCharsets.UTF_8);
                    return fillWorkshopTemplate(workshopTemplate);
                }
                case ONESHOT, CAMPAIGN, LONGSHOT -> {
                    String offerTemplate = new DefaultResourceLoader().getResource("classpath:META-INF/OfferTemplate").getContentAsString(StandardCharsets.UTF_8);
                    return fillOfferTemplate(offerTemplate);
                }
                default -> {
                    return "";
                }
            }
        } catch (IOException e) {
            return "";
        }
    }

    private String fillWorkshopTemplate(String template) {
        template = template.replace(TPGAMEMASTER, getUserAsTag(getGameMaster()));
        template = template.replace(TPTITLE, getTitle());
        template = template.replace(TPDESC, getDescript());
        template = template.replace(TPSIZE, getSize());
        if (isFixedDate()) {
            template = template.replace(TPPLAYDATE, WSPLAYDATETITLE + getPlayDate());
        } else {
            template = template.replace(TPPLAYDATE, "\n");
        }
        template = template.replace(TPADDITIONALS, getAdditionals());
        template = template.replace(TPLENGHT, getLength());
        template = template.replace(TPHOWJOIN, getHowJoin());
        template = template.replace("{{WorkshopRole}}", "<@" + Role.WORKSHOP_INTERESTS.getId() + ">");
        if (isCollect()) {
            template = template.replace(TPCOLLECTDATE, getCollectionDate());
        } else {
            template = template.replace(TPCOLLECTDATE, FULL);
        }

        return template;
    }

    private String getUserAsTag(Long userID) {
        return "<@!" + userID.toString() + ">";
    }

    private String getAdditionals() {
        StringBuilder sb = new StringBuilder();
        if (haveAdditionals || gameType == GameType.WORKSHOP) {
            sb.append(INDENT);
            sb.append(this.additionals.replace("\r", ("\r\u3000")));
        }
        return sb.toString();
    }

    private String fillOfferTemplate(String template) {
        template = template.replace(TPGAMETYPE, getGameTypeTitle());
        template = template.replace(TPGAMEMASTER, getUserAsTag(getGameMaster()));
        template = template.replace(TPSYSTEM, this.gameSystem);
        template = template.replace(TPSIZE, getSize());
        if (isFixedDate()) {
            template = template.replace(TPPLAYDATE, PLAYDATETITLE + getPlayDate() + "\n");
        } else {
            template = template.replace(TPPLAYDATE, "");
        }
        template = template.replace(TPHOW, getHowPlay());
        template = template.replace(TPLENGHT, getLength());
        template = template.replace(TPSERIOUSNESS, getSeriousness());
        template = template.replace(TPTITLE, getTitle());
        template = template.replace(TPDESC, getDescript());
        template = template.replace(TPTRIGGER, getTriggerList());
        template = template.replace(TPHOWJOIN, getHowJoin());
        template = template.replace(TPWHO, getWhoCanJoinAndValue());
        if (isHaveAdditionals()) {
            template = template.replace(TPADDITIONALS, ADDITIONALSTITLE + getAdditionals());
        } else {
            template = template.replace(TPADDITIONALS, "");
        }
        if (isCollect()) {
            template = template.replace(TPCOLLECTDATE, getCollectionDate());
        } else {
            template = template.replace(TPCOLLECTDATE, FULL);
        }

        return template;
    }

    private String getGameTypeTitle() {
        return switch (this.gameType) {
            case CAMPAIGN -> "***[ K A M P A I G N ]***";
            case ONESHOT -> "***[ O N E S H O T ]***";
            case WORKSHOP -> "***[ W O R K S H O P ]***";
            default -> "";
        };
    }

    private String getWhoCanJoinAndValue() {
        String joinText = ">nen d\u00fcrfen teilnehmen!";
        switch (this.whoCanJoin) {
            case ALL -> {
                return (Role.ALL.getId() + " darf teilnehmen!");
            }
            case NEWBIES -> {
                return "<@&" + Role.NEWBIES.getId() + ">e, " + "<@&" + Role.BEGINNER.getId() + ">nen und <@&" + Role.ADVANCED.getId() + joinText;
            }
            case BEGINNER -> {
                return "<@&" + Role.BEGINNER.getId() + ">nen und <@&" + Role.ADVANCED.getId() + joinText;
            }
            case ADVANCED -> {
                return "<@&" + Role.ADVANCED.getId() + joinText;
            }
            default -> {
                return "@everyone darf teilnehmen!";
            }
        }
    }

    public String toChannelDescString() {
        try {
            switch (this.gameType) {
                case WORKSHOP -> {
                    String workshopTemplate = new DefaultResourceLoader().getResource("classpath:META-INF/ChannelWorkshopTemplate").getContentAsString(StandardCharsets.UTF_8);
                    return fillWorkshopTemplate(workshopTemplate);
                }
                case ONESHOT, CAMPAIGN, LONGSHOT -> {
                    String offerTemplate = new DefaultResourceLoader().getResource("classpath:META-INF/ChannelOfferTemplate").getContentAsString(StandardCharsets.UTF_8);
                    return fillOfferTemplate(offerTemplate);
                }
                default -> {
                    return "";
                }
            }
        } catch (IOException e) {
            return "";
        }
    }

    public MessageEmbed toStringAsEmbed() {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle(this.gameType.getName() + ": " + this.title);
        eb.setDescription(toEmbedDescString());
        eb.setColor(this.gameType.getColor());
        return eb.build();
    }

    public String toEmbedDescString() {
        try {
            switch (this.gameType) {
                case WORKSHOP -> {
                    String workshopTemplate = new DefaultResourceLoader().getResource("classpath:META-INF/EmbedWorkshopTemplate").getContentAsString(StandardCharsets.UTF_8);
                    return fillWorkshopTemplate(workshopTemplate);
                }
                case ONESHOT, CAMPAIGN, LONGSHOT -> {
                    String offerTemplate = new DefaultResourceLoader().getResource("classpath:META-INF/EmbedOfferTemplate").getContentAsString(StandardCharsets.UTF_8);
                    return fillOfferTemplate(offerTemplate);
                }
                default -> {
                    return "";
                }
            }
        } catch (IOException e) {
            return "";
        }
    }


}
