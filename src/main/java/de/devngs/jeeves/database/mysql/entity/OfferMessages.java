package de.devngs.jeeves.database.mysql.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Entity
public class OfferMessages {
    @Id
    private UUID offerId;
    private Long messageId;
    private Long channelId;
}
