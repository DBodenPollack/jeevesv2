package de.devngs.jeeves.database.mysql.service.implementations;


import de.devngs.jeeves.database.mysql.entity.SurveyDelProb;
import de.devngs.jeeves.database.mysql.repository.SurveyDelProbRepository;
import de.devngs.jeeves.database.mysql.service.interfaces.SurveyDelProbComp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Transactional
@Component
public class SurveyDelProbCompImpl implements SurveyDelProbComp {

    @Autowired
    private SurveyDelProbRepository surveyDelProbRepository;

    @Override
    public List<SurveyDelProb> findAll() {
        return surveyDelProbRepository.findAll();
    }

    @Override
    public SurveyDelProb saveSurveyVote(SurveyDelProb surveyVote) {
        return surveyDelProbRepository.save(surveyVote);
    }

    @Override
    public void deleteSurveyVote(SurveyDelProb surveyVote) {
        surveyDelProbRepository.delete(surveyVote);
    }

    @Override
    public List<SurveyDelProb> findBySurveyId(UUID surveyId) {
        List<SurveyDelProb> tmp =  surveyDelProbRepository.findAll();
        List<SurveyDelProb> ret = new ArrayList<>();
        for(SurveyDelProb sv : tmp){
            if (sv.getSurveyId().equals(surveyId)){
                ret.add(sv);
            }
        }
        return ret;
    }

    @Override
    public void deleteAllVotesBySurveyId(UUID surveyId) {
        surveyDelProbRepository.deleteAllBySurveyId(surveyId);
    }
}
