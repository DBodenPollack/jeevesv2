package de.devngs.jeeves.database.mysql.repository;

import de.devngs.jeeves.database.mysql.entity.Survey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;


@Repository
public interface SurveyRepository extends JpaRepository<Survey, UUID> {
    Optional<Survey> findSurveyById(UUID id);
}
