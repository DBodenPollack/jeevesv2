package de.devngs.jeeves.database.mysql.service.interfaces;

import de.devngs.jeeves.database.mysql.entity.Survey;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SurveyComp {
    List<Survey> findAll();
    Optional<Survey> findSurveyById(UUID id);
    Survey saveSurvey(Survey survey);
    void deleteSurveyById(UUID id);
    void deleteSurvey(Survey survey);
}