package de.devngs.jeeves.database.mysql.service.interfaces;


import de.devngs.jeeves.database.mysql.entity.Offer;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfferComp {
    List<Offer> findAll();
    Optional<Offer> findOfferById(UUID id);
    List<Offer> findByPosted(boolean posted);
    Offer saveOffer(Offer offer);
    void deleteOfferById(UUID id);
    void deleteOffer(Offer offer);
}
