package de.devngs.jeeves.database.mysql.repository;

import de.devngs.jeeves.database.mysql.entity.SurveyVote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;


@Repository
public interface SurveyVoteRepository extends JpaRepository<SurveyVote, UUID> {
    Optional<SurveyVote> findSurveyVoteBySurveyId(UUID surveyId);
    void deleteAllBySurveyId(UUID surveyId);
}
