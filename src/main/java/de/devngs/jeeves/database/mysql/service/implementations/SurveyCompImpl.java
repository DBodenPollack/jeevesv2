package de.devngs.jeeves.database.mysql.service.implementations;

import de.devngs.jeeves.database.mysql.entity.Survey;
import de.devngs.jeeves.database.mysql.repository.SurveyRepository;
import de.devngs.jeeves.database.mysql.service.interfaces.SurveyComp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Transactional
@Component
public class SurveyCompImpl implements SurveyComp {

    private final SurveyRepository surveyRepository;

    @Autowired
    public SurveyCompImpl(SurveyRepository surveyRepository){
        this.surveyRepository = surveyRepository;
    }

    @Override
    public List<Survey> findAll() {
        return surveyRepository.findAll();
    }

    @Override
    public Optional<Survey> findSurveyById(UUID id) {
        return surveyRepository.findSurveyById(id);
    }

    @Override
    public Survey saveSurvey(Survey survey) {
        return surveyRepository.save(survey);
    }

    @Override
    public void deleteSurveyById(UUID id) {
        Optional<Survey> survey = surveyRepository.findSurveyById(id);
        survey.ifPresent(surveyRepository::delete);
    }

    @Override
    public void deleteSurvey(Survey survey) {
        surveyRepository.delete(survey);
    }
}