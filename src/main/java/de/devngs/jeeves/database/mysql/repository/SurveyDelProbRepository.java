package de.devngs.jeeves.database.mysql.repository;

import de.devngs.jeeves.database.mysql.entity.SurveyDelProb;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;


@Repository
public interface SurveyDelProbRepository extends JpaRepository<SurveyDelProb, UUID> {
    Optional<SurveyDelProb> findSurveyDelProbBySurveyId(UUID surveyId);
    void deleteAllBySurveyId(UUID surveyId);
}
