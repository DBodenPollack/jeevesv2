package de.devngs.jeeves.database.mysql.repository;

import de.devngs.jeeves.database.mysql.entity.Offer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface OfferRepository extends JpaRepository<Offer, UUID> {
    Optional<Offer> findOfferByOfferId(UUID offerId);
    List<Offer> findOfferByPosted(boolean posted);
}
