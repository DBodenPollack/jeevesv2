package de.devngs.jeeves.database.mysql.service.implementations;

import de.devngs.jeeves.database.mysql.entity.OfferMessages;
import de.devngs.jeeves.database.mysql.repository.OfferMessageRepository;
import de.devngs.jeeves.database.mysql.service.interfaces.OfferMessageComp;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Transactional
@Component
public class OfferMessageCompImpl implements OfferMessageComp {

    @Autowired
    private OfferMessageRepository offerMessageRepository;

    @Override
    public List<OfferMessages> getAllOfferMessages() {
        return offerMessageRepository.findAll();
    }

    @Override
    public OfferMessages getOfferMessageByOfferId(UUID id) {
        Optional<OfferMessages> offr = offerMessageRepository.findOfferMessagesByOfferId(id);
        return offr.orElseGet(OfferMessages::new);
    }

    @Override
    public OfferMessages saveOfferMessage(OfferMessages offerMessages) {
        return offerMessageRepository.save(offerMessages);
    }
}
