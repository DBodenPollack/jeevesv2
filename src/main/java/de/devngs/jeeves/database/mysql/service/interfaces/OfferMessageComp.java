package de.devngs.jeeves.database.mysql.service.interfaces;

import de.devngs.jeeves.database.mysql.entity.OfferMessages;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public interface OfferMessageComp {
    List<OfferMessages> getAllOfferMessages();
    OfferMessages getOfferMessageByOfferId(UUID id);
    OfferMessages saveOfferMessage(OfferMessages offerMessages);
}
