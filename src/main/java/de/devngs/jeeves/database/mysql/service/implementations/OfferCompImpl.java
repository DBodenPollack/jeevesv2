package de.devngs.jeeves.database.mysql.service.implementations;

import de.devngs.jeeves.database.mysql.entity.Offer;
import de.devngs.jeeves.database.mysql.repository.OfferRepository;
import de.devngs.jeeves.database.mysql.service.interfaces.OfferComp;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Transactional
@Component
public class OfferCompImpl implements OfferComp {
    @Autowired
    private OfferRepository offerRepository;

    @Override
    public List<Offer> findAll() {
        return offerRepository.findAll();
    }

    @Override
    public List<Offer> findByPosted(boolean posted){
        return offerRepository.findOfferByPosted(posted);
    }

    @Override
    public Optional<Offer> findOfferById(UUID id) {
        return offerRepository.findOfferByOfferId(id);
    }

    @Override
    public Offer saveOffer(Offer offer) {
        return offerRepository.save(offer);
    }

    @Override
    public void deleteOfferById(UUID id) {
        offerRepository.deleteById(id);
    }

    @Override
    public void deleteOffer(Offer offer) {
        offerRepository.delete(offer);
    }


}
