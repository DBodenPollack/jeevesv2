package de.devngs.jeeves.database.mysql.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;


@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class SurveyVoteKeys  implements Serializable {
    private UUID surveyId;
    private Long userId;
}
