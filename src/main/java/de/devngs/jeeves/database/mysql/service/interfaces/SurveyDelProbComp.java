package de.devngs.jeeves.database.mysql.service.interfaces;

import de.devngs.jeeves.database.mysql.entity.SurveyDelProb;

import java.util.List;
import java.util.UUID;

public interface SurveyDelProbComp {
    List<SurveyDelProb> findAll();
    SurveyDelProb saveSurveyVote(SurveyDelProb surveyVote);
    void deleteSurveyVote(SurveyDelProb surveyVote);

    List<SurveyDelProb> findBySurveyId(UUID surveyId);

    void deleteAllVotesBySurveyId(UUID surveyId);
}