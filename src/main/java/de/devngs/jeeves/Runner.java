package de.devngs.jeeves;

import de.devngs.jeeves.bot.additionals.enums.Server;
import de.devngs.jeeves.handler.CommandHandler;
import de.devngs.jeeves.utils.LogWrapper;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.ChunkingFilter;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Runner {
    private static ClassPathXmlApplicationContext ctx;
    private static JDA jda;
    private static Server server;

    //devKey ODg4NTM0MzcwMjQ2MzM2NTEy.YUUGHg.Mqq2iLdKwo9QOIJyUrXbFrevrNE
    public static void main(final String[] args) {

        if (args.length == 0) {

            LogWrapper.logSevere("You have to provide a token as first argument!");
            System.exit(1);
        }

        ctx = new ClassPathXmlApplicationContext("classpath:META-INF/spring.xml");

        String dcID = args[0];

        jda = JDABuilder.createDefault(dcID)
                .setChunkingFilter(ChunkingFilter.ALL)
                .setMemberCachePolicy(MemberCachePolicy.ALL)
                .enableIntents(GatewayIntent.GUILD_MEMBERS)
                .build();

        if (dcID.equals("ODg4NTM0MzcwMjQ2MzM2NTEy.YUUGHg.Mqq2iLdKwo9QOIJyUrXbFrevrNE")){
            server = Server.DEMOCHANNEL;
        } else if (dcID.equals("OTQyMDA2NTExMTQ2NzYyMjgw.YgeN7Q.4c64xlQ7Uvx7_0XQ1zdlf6dh5es")) {
            server = Server.MITNERMENGEFANTASIE;
        }

        CommandHandler.initCommands();

    }

    public static JDA getJda(){
        return jda;
    }
    public static ClassPathXmlApplicationContext getCTX(){
        return ctx;
    }
    public static Server getServer(){
        return server;
    }
}
