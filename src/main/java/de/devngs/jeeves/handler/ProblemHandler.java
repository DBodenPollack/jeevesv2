package de.devngs.jeeves.handler;

import de.devngs.jeeves.bot.additionals.enums.Channel;
import de.devngs.jeeves.utils.LogWrapper;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;

public class ProblemHandler {
    private ProblemHandler(){}
    public static void contactMods(Guild guild, String message){
        TextChannel modArea = guild.getTextChannelById(Channel.MODAREA.getIdAsLong());
        if (modArea == null) {
            LogWrapper.logSevere("Modarea could not found");
            return;
        }
        modArea.sendMessage(message).queue();
    }
}
