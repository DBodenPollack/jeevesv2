package de.devngs.jeeves.handler;

import de.devngs.jeeves.Runner;
import de.devngs.jeeves.bot.additionals.enums.Command;
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction;

public class CommandHandler {
    private CommandHandler() {
        throw new IllegalStateException("Utility class");
    }

    public static void initCommands() {
        CommandListUpdateAction commands = Runner.getJda().updateCommands();
        commands.addCommands(Command.getAllCommands()).queue();

        for (Command cmd : Command.values()) {
            Runner.getJda().addEventListener(cmd.listenerAdapter());
        }

    }

}
