package de.devngs.jeeves;

import de.devngs.jeeves.bot.additionals.handler.DiceHandler;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DiceHandlerTest {
    @Test
    void rolledDiceShouldBeInRangeBetweenOneAndTwenty()
    {
        int min = 1;
        int max = 20;
        int r = DiceHandler.rollDice(min, max);
        assertTrue(r <= max && r >= min);
    }


}