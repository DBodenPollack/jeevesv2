package de.devngs.jeeves;

import de.devngs.jeeves.bot.additionals.handler.CalcHandler;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalcHandlerTest {
    @Test
    void calculationShouldBeTwentyFive() {
        assertEquals("25.0", CalcHandler.calc("12,5x8/(2+2)"));
    }

    @Test
    void calculationShouldFourtyThree() {
        assertEquals("43.0", CalcHandler.calc("2 x (2 + (41 - 2) / 2)"));
    }

    @Test
    void calculationShouldBeTwoHundredSixtyPointSix() {
        assertEquals("260.8", CalcHandler.calc("(4,5 + 2,3) x 41-(2+16)"));
    }
}